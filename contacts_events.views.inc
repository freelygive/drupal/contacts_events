<?php

/**
 * @file
 * Views hook implementations for Contacts Events.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function contacts_events_views_data() {
  $data['commerce_order']['booking_link'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup("Continue booking link"),
    'real field' => 'order_id',
    'field' => ['id' => 'contacts_events_booking_links'],
  ];

  $data['commerce_order']['booking_relation_icons'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup('Booking relation icons'),
    'help' => new TranslatableMarkup("Icons indicating the user's relationship to the booking."),
    'real field' => 'order_id',
    'field' => ['id' => 'contacts_events_booking_relation_icons'],
  ];

  $data['commerce_order']['contacts_events_ticketholder_ticket_status'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup('Ticket State'),
    'help' => new TranslatableMarkup("Ticket state for the current user."),
    'real field' => 'order_id',
    'field' => ['id' => 'contacts_events_ticketholder_ticket_status'],
  ];

  $data['commerce_order']['user_bookings'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup("User's bookings"),
    'help' => new TranslatableMarkup('Show all bookings for a given user ID, both booking manager and ticket holders.'),
    'real field' => 'order_id',
    'argument' => ['id' => 'contacts_events_user_bookings'],
  ];

  $data['commerce_order']['contacts_events_balance'] = [
    'title' => t('Order balance'),
    'field' => [
      'title' => t('Order balance'),
      'help' => t('Displays the remaining balance for an order.'),
      'id' => 'contacts_events_balance',
      'real field' => 'order_id',
    ],
  ];

  // Add field to order for "Ticketholder Names".
  $data['commerce_order']['ticketholder_names'] = [
    'title' => new TranslatableMarkup('Ticket holder names'),
    'help' => new TranslatableMarkup('Names of all ticketholders in this booking'),
    'real field' => 'order_id',
    'field' => ['id' => 'contacts_events_booking_ticket_holders'],
  ];

  $data['commerce_order_item__state']['table'] = [
    'group' => new TranslatableMarkup('Order item'),
    'join' => [
      'commerce_order_item' => [
        'left_field' => 'order_item_id',
        'field' => 'entity_id',
        'extra' => [
          [
            'field' => 'deleted',
            'value' => 0,
            'numberic' => TRUE,
          ],
        ],
      ],
    ],
    'entity type' => 'commerce_order_item',
  ];

  $data['commerce_order_item__state']['state_value'] = [
    'group' => new TranslatableMarkup('Order item'),
    'title' => new TranslatableMarkup('State'),
    'field' => [
      'id' => 'field',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'entity field' => 'state',
  ];

  // OrderItem::$confirmed.
  $data['commerce_order_item__confirmed']['table'] = [
    'group' => new TranslatableMarkup('Order item'),
    'join' => [
      'commerce_order_item' => [
        'left_field' => 'order_item_id',
        'field' => 'entity_id',
        'extra' => [
          [
            'field' => 'deleted',
            'value' => 0,
            'numberic' => TRUE,
          ],
        ],
      ],
    ],
    'entity type' => 'commerce_order_item',
  ];
  $data['commerce_order_item__confirmed']['confirmed'] = [
    'group' => new TranslatableMarkup('Order item'),
    'title' => new TranslatableMarkup('Confirmed'),
    'field' => [
      'id' => 'field',
      'field_name' => 'confirmed',
      'entity_type' => 'contacts_ticket',
      'real field' => 'confirmed_value',
      'additional fields' => [
        'delta',
        'langcode',
        'bundle',
        'confirmed_value',
      ],
      'element type' => 'div',
      'is revision' => FALSE,
      'click sortable' => TRUE,
    ],
  ];
  $data['commerce_order_item__confirmed']['confirmed_value'] = [
    'group' => new TranslatableMarkup('Order item'),
    'title' => new TranslatableMarkup('Confirmed'),
    'filter' => [
      'id' => 'date',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'confirmed_value',
      ],
      'field_name' => 'confirmed',
      'entity_type' => 'contacts_ticket',
      'allow empty' => TRUE,
    ],
    'sort' => [
      'id' => 'date',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'confirmed_value',
      ],
      'field_name' => 'confirmed',
      'entity_type' => 'contacts_ticket',
    ],
  ];

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function contacts_events_views_data_alter(array &$data) {
  $data['commerce_order_item__state']['state_value']['filter']['id'] = 'contacts_state_machine_state';
  $data['commerce_order__event']['event_target_id']['filter']['id'] = 'contacts_events_order_event';

  $data['users_field_data']['user_to_ticket'] = [
    'relationship' => [
      'title' => new TranslatableMarkup('Tickets for a User'),
      'label' => new TranslatableMarkup('Ticket'),
      'id' => 'standard',
      'base' => 'contacts_ticket',
      'base field' => 'contact',
      'entity type' => 'contacts_ticket',
      'relationship field' => 'uid',
      'real field' => 'contact',
    ],
  ];
}
