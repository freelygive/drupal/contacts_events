<?php

namespace Drupal\village_allocation;

use Drupal\commerce_order\Entity\Order;

/**
 * This is a wrapper around Village Groups and Bookings.
 *
 * This provides a format that is more useful for the village allocation screen.
 *
 * @package Drupal\contacts_events_villages
 */
class VillageAllocationGroup {

  /**
   * The ID of this group.
   *
   * @var int
   */
  private $groupId;

  /**
   * The name of this group.
   *
   * @var string
   */
  private $groupName;

  /**
   * Bookings in this group.
   *
   * @var \Drupal\commerce_order\Entity\Order[]
   */
  private $bookings = [];

  /**
   * Array of village IDs associated with this group.
   *
   * @var int[]
   */
  private $villages = [];

  /**
   * Array of group names in the same link set, keyed by group ID.
   *
   * @var array
   */
  private $links = [];

  /**
   * VillageAllocationGroup constructor.
   *
   * @param int $group_id
   *   ID of village allocation group.
   * @param string $group_name
   *   Name of group.
   */
  public function __construct($group_id, $group_name) {
    $this->groupId = $group_id;
    $this->groupName = $group_name;
  }

  /**
   * Adds a booking to this group.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order (booking) to add.
   * @param int $pitches
   *   Number of pitches required by this booking.
   * @param bool $is_village_host_booking
   *   Whether the village host is part of this order.
   * @param array $child_bookings
   *   Array of child bookings. Each element is an array with the following keys
   *   order: the corresponding Order entity.
   *   pitches: number of pitches required (int).
   */
  public function addBooking(Order $order, $pitches = 0, $is_village_host_booking = FALSE, array $child_bookings = []) {
    $requirements = [];

    // Take into account any Requirements on the parent Group Booking.
    foreach ($order->get('village_requirements') as $requirement) {
      $requirements[] = [
        'name' => $requirement->entity->getName(),
        'id' => $requirement->entity->id(),
        'icon' => $requirement->entity->get('icon')->value,
      ];
    }

    // Also take into account any requirements on the child bookings.
    foreach ($child_bookings as $child_booking) {
      /** @var \Drupal\commerce_order\Entity\Order $child_order */
      $child_order = $child_booking['order'];

      foreach ($child_order->get('village_requirements') as $requirement) {
        $requirements[] = [
          'name' => $requirement->entity->getName(),
          'id' => $requirement->entity->id(),
          'icon' => $requirement->entity->get('icon')->value,
        ];
      }
    }

    if ($is_village_host_booking) {
      $requirements[] = [
        'name' => 'Village Host',
        'id' => 0,
        'icon' => 'village-host',
      ];
    }

    $this->bookings[] = [
      'orderNumber' => $order->getOrderNumber(),
      'id' => $order->id(),
      'village' => $order->get('village')->target_id,
      'pitches' => $pitches,
      'specialRequirements' => $requirements,
      'selected' => FALSE,
      'state' => $order->getState()->getId(),
      'bookingManager' => [
        'id' => $order->getCustomer()->id(),
        'name' => $order->getCustomer()->getDisplayName(),
      ],
      'childBookings' => array_map(function ($child_booking) {
        /** @var \Drupal\commerce_order\Entity\Order $child_order */
        $child_order = $child_booking['order'];
        return [
          'orderNumber' => $child_order->getOrderNumber(),
          'id' => $child_order->id(),
          'village' => $child_order->get('village')->target_id,
          'pitches' => $child_booking['pitches'],
          'state' => $child_order->getState()->getId(),
          'bookingManager' => [
            'id' => $child_order->getCustomer()->id(),
            'name' => $child_order->getCustomer()->getDisplayName(),
          ],
        ];
      }, $child_bookings),
    ];
  }

  /**
   * Adds a linked group.
   *
   * @param int $linked_group_id
   *   Linked group ID.
   * @param string $linked_group_name
   *   Linked group name.
   */
  public function addLink(int $linked_group_id, string $linked_group_name) {
    // Parameter must be type hinted as int, to ensure any string group IDs are
    // converted to ints (which the javascript expects).
    $this->links[] = [
      'id' => $linked_group_id,
      'name' => $linked_group_name,
    ];
  }

  /**
   * Gets the name of the group.
   *
   * @return string
   *   The name of the group.
   */
  public function getName() {
    return $this->groupName;
  }

  /**
   * Gets the ID of the group.
   *
   * @return int
   *   The ID of the group.
   */
  public function id() {
    return $this->groupId;
  }

  /**
   * Converts the group to an array, ready for JSON serialization.
   *
   * @return array
   *   Array of group and bookings.
   */
  public function toArray() {
    $result = [
      'id' => (int) $this->groupId,
      'name' => $this->groupName,
      'bookings' => $this->bookings,
      'pitches' => 0,
      'villages' => $this->villages,
      'links' => $this->links,
    ];

    foreach ($this->bookings as $booking) {
      $result['pitches'] += $booking['pitches'];
    }

    return $result;
  }

  /**
   * Records that this group is already partly allocated to a village.
   *
   * @param int $village_id
   *   The ID of the village.
   */
  public function addVillage($village_id) {
    $this->villages[] = $village_id;
  }

  /**
   * Villages that this group has been assigned to.
   *
   * @return int[]
   *   Array of village ids.
   */
  public function getVillages() : array {
    return $this->villages;
  }

  /**
   * Sets the villages to which this group has already been assigned.
   *
   * @param array $villages
   *   Array of village IDs.
   */
  public function setVillages(array $villages) {
    $this->villages = $villages;
  }

  /**
   * Gets the number of bookings in this group.
   *
   * @return int
   *   Number of bookings.
   */
  public function numberOfBookings() : int {
    return count($this->bookings);
  }

}
