<?php

namespace Drupal\village_allocation\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides bookings list.
 *
 * Note that we have 2 URI paths. The first one (with the drupal.org...link) is
 * for drupal 8 compatibility. The second "create" one is for Drupal 9.
 * These uri path names were renamed between 8 and 9.
 *
 * @RestResource(
 *   id = "village_allocation_allocate",
 *   label = @Translation("Village Allocation - Allocate"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/admin/village_allocation/allocate/{event_id}",
 *     "create" = "/admin/village_allocation/allocate/{event_id}"
 *   }
 * )
 */
class AllocateVillageResource extends ResourceBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Village group indexer.
   *
   * @var \Drupal\village_allocation\VillageGroupIndexer
   */
  protected $indexer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $self = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $self->entityTypeManager = $container->get('entity_type.manager');
    $self->indexer = $container->get('village_allocation.indexer');
    return $self;
  }

  /**
   * Allocates village on POST.
   *
   * @param string $event_id
   *   Event ID.
   * @param array $data
   *   POST data. Array containing booking ids and village id.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   Success indication.
   */
  public function post($event_id, array $data) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $orders = $this->entityTypeManager->getStorage('commerce_order')->loadMultiple($data['booking_ids']);

    $groups_to_reindex = [];

    foreach ($orders as $order) {
      $order->set('village', $data['village_id']);
      // Usually village_allocation_commerce_order_update will queue the group
      // for re-indexing when the order is saved. This won't be soon enough, as
      // we need the index to be updated immediately in time for the next
      // request. Set the skipGroupIndexing surrogate field,
      // which will cause the indexing not to run when the order is saved.
      // Instead we explicitly index the groups below.
      $order->skipGroupIndexing = TRUE;
      $order->save();

      /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $group */
      $group = $order->get('village_group')->entity;
      if (!isset($groups_to_reindex[$group->id()])) {
        $groups_to_reindex[$group->id()] = $group;
      }
    }

    $this->indexer->reindexGroupsImmediately($groups_to_reindex);

    return new ModifiedResourceResponse(['success' => TRUE]);
  }

}
