<?php

namespace Drupal\village_allocation\Plugin\rest\resource;

use Drupal\contacts_events_villages\Entity\Village;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\village_allocation\VillageAllocationQueries;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides bookings list.
 *
 * @RestResource(
 *   id = "village_allocation_villages",
 *   label = @Translation("Village Allocation Villages"),
 *   uri_paths = {
 *     "canonical" = "/admin/village_allocation/villages/{event_id}"
 *   }
 * )
 */
class VillagesResource extends ResourceBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * VA queries.
   *
   * @var \Drupal\village_allocation\VillageAllocationQueries
   */
  protected $queries;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, Connection $db, VillageAllocationQueries $queries) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityTypeManager = $entity_type_manager;
    $this->db = $db;
    $this->queries = $queries;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('village_allocation.queries')
    );
  }

  /**
   * Gets villages.
   *
   * @param string $event_id
   *   Event ID.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Response containing serialized villages.
   */
  public function get($event_id) {
    // Load villages list.
    $villages = $this->queries->getVillagesInOrder($event_id);
    $results = [];

    // Project the results into a sensible format. Standard drupal entities
    // really don't represent well as JSON.
    /** @var \Drupal\contacts_events_villages\Entity\Village $village */
    foreach ($villages as $village) {
      $projection = [
        'id' => $village->id(),
        'name' => $village->getName(),
        'colour' => $village->get('colour')->value,
        'event' => $village->getEventId(),
        'fillValue' => (float) $village->get('fill_value')->value,
        'gate' => $village->get('gate')->value,
        'notes' => $village->get('notes')->value,
        'pitches' => (float) $village->get('pitches')->value,
        // pitches_allocated is a surrogate field added by getVillagesInorder.
        'pitchesAllocated' => $village->pitches_allocated,
        'specialRequirements' => $this->getSpecialRequirements($village),
      ];
      $results[] = $projection;
    }

    // Result of query is an associative array. We want non-associative
    // otherwise it'll be serialized as a JS object instead of a JS array.
    return (new ResourceResponse($results))
      ->addCacheableDependency([
        '#cache' => ['max-age' => 0],
      ]);
  }

  /**
   * Get special requirements.
   *
   * @param \Drupal\contacts_events_villages\Entity\Village $village
   *   The village to get special requirements for.
   *
   * @return array
   *   Array of arrays, where each child array contains keys name, id, icon.
   */
  private function getSpecialRequirements(Village $village) {
    $results = [];

    foreach ($village->get('special_requirements') as $requirement_item) {
      $requirement = $requirement_item->entity;
      /** @var \Drupal\taxonomy\Entity\Term $requirement */
      $results[] = [
        'name' => $requirement->getName(),
        'id' => $requirement->id(),
        'icon' => $requirement->get('icon')->value,
      ];
    }
    return $results;
  }

}
