<?php

namespace Drupal\contacts_events_teams\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Team application type entity.
 *
 * @ConfigEntityType(
 *   id = "c_events_team_app_type",
 *   label = @Translation("Team Application Form"),
 *   label_plural = @Translation("Team Application Forms"),
 *   label_collection = @Translation("Team Application Forms"),
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_events_teams\TeamApplicationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\contacts_events_teams\Form\TeamApplicationTypeForm",
 *       "edit" = "Drupal\contacts_events_teams\Form\TeamApplicationTypeForm",
 *       "delete" = "Drupal\contacts_events_teams\Form\TeamApplicationTypeDeleteForm",
 *       "clone" = "Drupal\contacts_events_teams\Form\TeamApplicationTypeCloneForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_teams\TeamApplicationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "c_events_team_app_type",
 *   admin_permission = "administer contacts events team apps",
 *   bundle_of = "c_events_team_app",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/teams/forms/add",
 *     "edit-form" = "/admin/structure/teams/forms/{c_events_team_app_type}",
 *     "delete-form" = "/admin/structure/teams/forms/{c_events_team_app_type}/delete",
 *     "clone-form" = "/admin/structure/teams/forms/{c_events_team_app_type}/clone",
 *     "collection" = "/admin/structure/teams/forms",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class TeamApplicationType extends ConfigEntityBundleBase {

  /**
   * The application form id.
   *
   * @var string
   */
  protected $id;

  /**
   * The application form name.
   *
   * @var string
   */
  protected $label;

}
