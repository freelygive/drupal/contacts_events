<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\contacts_events_teams\TeamQueries;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Helpful methods for working with team application step forms.
 *
 * @package Drupal\contacts_events_teams\Plugin\TeamApplicationStep
 */
trait TeamStepsTrait {

  /**
   * The team queries service.
   *
   * @var \Drupal\contacts_events_teams\TeamQueries
   */
  protected $queries;

  /**
   * The ticket the applicaiton is for.
   *
   * @var \Drupal\contacts_events\Entity\TicketInterface
   */
  protected $ticket;

  /**
   * Get the team queries service.
   *
   * @return \Drupal\contacts_events_teams\TeamQueries
   *   The team queries service.
   */
  public function getQueries(): TeamQueries {
    if (!isset($this->queries)) {
      $this->setQueries(\Drupal::service('contacts_events_teams.queries'));
    }
    return $this->queries;
  }

  /**
   * Set the team queries service.
   *
   * @param \Drupal\contacts_events_teams\TeamQueries $queries
   *   The team queries service.
   *
   * @return $this
   */
  public function setQueries(TeamQueries $queries) {
    $this->queries = $queries;
    return $this;
  }

  /**
   * Gets the ticket we are applying for.
   *
   * @return \Drupal\contacts_events\Entity\TicketInterface
   *   The ticket.
   */
  protected function getTicket() {
    return $this->ticket;
  }

  /**
   * Set the ticket the application is for.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket.
   *
   * @return $this
   */
  public function setTicket(TicketInterface $ticket) {
    $this->ticket = $ticket;
    return $this;
  }

  /**
   * Gets the current team application from the ticket.
   *
   * @param bool $create
   *   Whether to create the application.
   *
   * @return \Drupal\contacts_events_teams\Entity\TeamApplication|null
   *   The current team application, or null if it's not been started yet.
   */
  protected function getTeamApplication($create = FALSE) {
    $ticket = $this->getTicket();
    $app = $this->getQueries()->getTeamApplicationForTicket($ticket);

    if (!$app && $create && !$ticket->get('team')->isEmpty()) {
      /** @var \Drupal\contacts_events_teams\Entity\TeamInterface $team */
      $team = $ticket->get('team')->entity;
      $app = $this->entityTypeManager
        ->getStorage('c_events_team_app')
        ->create([
          'type' => $team->get('form')->target_id,
          'team' => $team->id(),
          'ticket' => $ticket->id(),
          'event' => $ticket->getEvent()->id(),
          'user_id' => $ticket->getTicketHolderId(),
        ]);
    }

    return $app;
  }

  /**
   * Set the configuration for the form.
   *
   * @param array $configuration
   *   The configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration) {
    if ($this instanceof ContentEntityForm && isset($configuration['form_display'])) {
      $this->setOperation($configuration['form_display']);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Overrides the actions element on content entity forms and builds
   * next/previous buttons as required.
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_events_teams\Plugin\TeamApplicationStep\TeamApplicationStepManager $manager */
    $manager = \Drupal::service('plugin.manager.team_application_step');
    $route_match = \Drupal::routeMatch();
    $step = $route_match->getParameter('step');

    $previous_step = FALSE;

    // array_keys to just get plugin ids.
    $steps = array_keys($manager->getDefinitions());

    if ($step == NULL || reset($steps) === $step) {
      // Fist step.
    }
    else {
      $previous_step = $steps[array_search($step, $steps) - 1];
    }

    $actions = [
      '#type' => 'actions',
    ];

    if ($previous_step) {
      $actions['back'] = [
        '#type' => 'link',
        '#title' => new TranslatableMarkup('Back'),
        '#url' => Url::fromRoute('contacts_events_teams.application_flow', [
          'step' => $previous_step,
          'contacts_ticket' => $this->getTicket()->id(),
        ]),
        '#attributes' => [
          'class' => ['btn', 'btn-secondary'],
        ],
      ];
    }

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => new TranslatableMarkup('Continue'),
      '#submit' => ['::submitForm', '::save', '::nextStep'],
      '#attributes' => [
        'class' => ['button', 'button--primary'],
      ],
    ];

    // Only the steps that inherit from ContentEntityForm will have a
    // save method. Those that inherit directly from FormBase won't.
    if (!method_exists($this, 'save')) {
      unset($actions['submit']['#submit'][1]);
    }

    return $actions;
  }

  /**
   * Redirects to the next step in the application.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function nextStep(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_events_teams\Plugin\TeamApplicationStep\TeamApplicationStepManager $manager */
    $manager = \Drupal::service('plugin.manager.team_application_step');
    $route_match = \Drupal::routeMatch();
    $step = $route_match->getParameter('step');
    $steps = array_keys($manager->getDefinitions());

    $next_step = $steps[array_search($step, $steps) + 1];

    $form_state->setRedirect('contacts_events_teams.application_flow', [
      'step' => $next_step,
      'contacts_ticket' => $this->getTicket()->id(),
    ]);
  }

}
