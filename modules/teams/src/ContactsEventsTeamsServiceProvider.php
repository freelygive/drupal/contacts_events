<?php

namespace Drupal\contacts_events_teams;

use Drupal\contacts_events_teams\EventSubscriber\DbsTeamApplicationStateSubscriber;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Custom service provider for Contacts Events Teams.
 */
class ContactsEventsTeamsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['contacts_dbs'])) {
      $container->register('contacts_events.dbs_team_app_state_subscriber', DbsTeamApplicationStateSubscriber::class)
        ->addTag('event_subscriber')
        ->addArgument(new Reference('contacts_dbs.dbs_manager'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Replace the Commerce Checkout Order Manager with our custom version.
    $definition = $container->getDefinition('commerce_checkout.checkout_order_manager');
    $definition->setClass('Drupal\contacts_events\CustomCheckoutOrderManager');
  }

}
