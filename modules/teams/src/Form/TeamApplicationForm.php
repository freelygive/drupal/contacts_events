<?php

namespace Drupal\contacts_events_teams\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Team application edit form.
 *
 * @ingroup contacts_events_teams
 */
class TeamApplicationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $app */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Team application.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Team application.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));

    if ($this->moduleHandler->moduleExists('contacts_references')) {
      // Find any references and update their application & uid fields.
      /** @var \Drupal\contacts_references\Entity\Reference[] $references */
      $references = \Drupal::service('contacts_references.reference_field_helper')
        ->findReferences($this->entity);

      // @todo Move this to inline entity form process on reference creation.
      foreach ($references as $reference) {
        $reference->set('uid', $entity->getOwnerId());
        $reference->set('application', $entity->id());
        $reference->save();
      }
    }
  }

}
