<?php

namespace Drupal\contacts_events_teams\EventSubscriber;

use Drupal\contacts_events_teams\TeamEmailService;
use Drupal\contacts_events_teams\TeamQueries;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for sending Team emails.
 *
 * @package Drupal\contacts_events_teams\EventSubscriber
 */
class TeamEmailSubscriber implements EventSubscriberInterface {

  /**
   * The teams email config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Team email service.
   *
   * @var \Drupal\contacts_events_teams\TeamEmailService
   */
  protected $teamEmails;

  /**
   * Current user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * Team queries.
   *
   * @var \Drupal\contacts_events_teams\TeamQueries
   */
  protected $queries;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TeamEmailSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\contacts_events_teams\TeamEmailService $team_emails
   *   Team email sender.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\contacts_events_teams\TeamQueries $queries
   *   Queries.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TeamEmailService $team_emails, AccountProxyInterface $current_user, TeamQueries $queries, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config_factory->get('contacts_events_teams.emails');
    $this->teamEmails = $team_emails;
    $this->currentUser = $current_user;
    $this->queries = $queries;
    $this->logger = $logger_factory->get('contacts_events_teams');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['contacts_events_order_items.team_app_in_progress.post_transition'][] = ['sendApplicationRequest'];
    $events['contacts_events_teams_applications.approve.post_transition'][] = ['sendAcceptance'];
    $events['contacts_events_teams_applications.submit.post_transition'][] = ['sendSubmitted'];
    return $events;
  }

  /**
   * Send an request to apply for team.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function sendApplicationRequest(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $event->getEntity();
    /** @var \Drupal\contacts_events\Entity\TicketInterface $ticket */
    $ticket = $order_item->getPurchasedEntity();
    /** @var \Drupal\contacts_events_teams\Entity\TeamInterface|null */
    $team = $ticket->get('team')->entity;

    if ($this->queries->getTeamApplicationForTicket($ticket)) {
      // Don't send out the application request if there's already
      // a team application attached to this ticket.
      // This can happen in the scenario when a staff member changes which team
      // a ticketholder is applying for. In that scenario, we explicitly don't
      // want to send them a new email.
      return;
    }

    $this->teamEmails->sendToApplicant('application_request', $ticket, $team);
  }

  /**
   * Sends acceptance.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function sendAcceptance(WorkflowTransitionEvent $event) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $app */
    $app = $event->getEntity();
    $team = $app->getTeam();
    $ticket = $app->getTicket();
    if (!$team) {
      $this->logger->error('Failed to send acceptance email to application (ID: @application) as it does not have a team.', ['@application' => [$app->id()]]);
    }
    else {
      $this->teamEmails->sendToApplicant('application_accepted', $ticket, $team);
    }
  }

  /**
   * Sends the submitted email.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function sendSubmitted(WorkflowTransitionEvent $event) {
    // Only send the submitted email when the current user is the applicant.
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $app */
    $app = $event->getEntity();
    $team = $app->getTeam();
    $ticket = $app->getTicket();

    if ($this->currentUser->id() == $ticket->get('contact')->target_id) {
      if (!$team) {
        $this->logger->error('Failed to send application submitted email for application (ID: @application) as it does not have a team.', ['@application' => [$app->id()]]);
      }
      else {
        $this->teamEmails->sendToApplicant('application_submitted', $ticket, $team);
      }
    }
  }

}
