<?php

namespace Drupal\contacts_events_teams\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Contacts Events Teams route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Make sure the routes use the admin theme.
    $admin_routes = [
      'view.contacts_events_teams_applications_archived.all',
      'view.contacts_events_teams_applications_archived.team',
      'view.contacts_events_teams_applications_current.all',
      'view.contacts_events_teams_applications_current.team',
    ];
    foreach ($admin_routes as $route_name) {
      if ($route = $collection->get($route_name)) {
        $route->setOption('_admin_route', TRUE);
      }
    }

    // Adjust routes that should be using the view team access check.
    $team_access_routes = [
      'view.contacts_events_teams_applications_archived.team',
      'view.contacts_events_teams_applications_current.team',
      'view.contacts_events_teams_applications_current.data_export_2',
      'entity.c_events_team.canonical',
    ];
    foreach ($team_access_routes as $route_name) {
      if ($route = $collection->get($route_name)) {
        $requirements = $route->getRequirements();
        unset($requirements['_permission']);
        $route->setRequirements($requirements);
        $route->setRequirement('_entity_access', 'c_events_team.view');
      }
    }

    // Adjust parameters to make them place nicely with other tasks that require
    // named parameters.
    $view_routes = [
      'view.contacts_events_teams_applications_archived.all',
      'view.contacts_events_teams_applications_archived.team',
      'view.contacts_events_teams_applications_current.all',
      'view.contacts_events_teams_applications_current.team',
      'view.contacts_events_teams_applications_current.data_export_2',
    ];
    foreach ($view_routes as $route_name) {
      if ($route = $collection->get($route_name)) {
        // We may need to adjust the views argument map to make it play nice.
        if ($route->hasOption('_view_argument_map')) {
          $arg_map = $route->getOption('_view_argument_map');
          if ($arg_map['arg_0'] == 'arg_0') {
            $arg_map['arg_0'] = 'contacts_event';
            $arg_map['arg_1'] = 'c_events_team';
            $route->setOption('_view_argument_map', $arg_map);
            $route->setPath(strtr($route->getPath(), $arg_map));
          }
        }

        // Ensure the parameter up-casting is configured.
        if (!$route->hasOption('parameters')) {
          $route->setOption('parameters', [
            'contacts_event' => [
              'type' => 'entity:contacts_event',
              'converter' => 'paramconverter.entity',
            ],
            'c_events_team' => [
              'type' => 'entity:c_events_team',
              'converter' => 'paramconverter.entity',
            ],
          ]);
        }
      }
    }

    // Additional access check for routes based on event teams setting.
    $event_access_routes = [
      'view.contacts_events_teams_applications_archived.all',
      'view.contacts_events_teams_applications_archived.team',
      'view.contacts_events_teams_applications_current.all',
      'view.contacts_events_teams_applications_current.team',
      'view.contacts_events_teams_applications_current.data_export_2',
    ];
    foreach ($event_access_routes as $route_name) {
      if ($route = $collection->get($route_name)) {
        $route->setRequirement('_contacts_events_teams', 'TRUE');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
