<?php

namespace Drupal\contacts_events_teams\EventSubscriber;

use Drupal\contacts_events_teams\Entity\TeamInterface;

/**
 * Trait for common logic for updating references to teams after a clone.
 */
trait TeamsCloneReferenceTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Find the cloned teams for the new event.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamInterface[]|int[] $teams
   *   Either an array of team entities or team IDs from the source event.
   * @param int $event_id
   *   The target event ID.
   *
   * @return int[]
   *   An array of team IDs for the new event, preserving keys.
   */
  protected function findClonedTeams(array $teams, int $event_id): array {
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('c_events_team');

    $cloned_ids = [];
    foreach ($teams as $key => $team) {
      if (!($team instanceof TeamInterface)) {
        $team = $storage->load($team);
      }

      // Find the matching team for the new event. A match on name and category
      // should be sufficient.
      $team_ids = $storage->getQuery()
        ->accessCheck()
        ->condition('event', $event_id)
        ->condition('name', $team->get('name')->value)
        ->condition('category', $team->get('category')->target_id)
        ->range(0, 1)
        ->execute();

      // @todo Implement some error reporting for the clone process.
      if ($team_ids) {
        $cloned_ids[$key] = reset($team_ids);
      }
    }

    return $cloned_ids;
  }

}
