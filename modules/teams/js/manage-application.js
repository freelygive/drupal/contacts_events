/**
 * @file
 * Javascript behaviour for team apps.
 */

(function ($) {
  $(function () {
    $('.team-tab-link').click(function() {
      // Close all tabs.
      $('.team-tab').hide();
      // Remove active class from current tab link.
      $('.team-tab-link').removeClass('active');
      // Show the target
      $('#' + $(this).data('tab')).show();
      $(this).addClass('active');
    });

    // Show the first tab on page load.
    $('.team-tab').first().show();
    $('.team-tab-link').first().addClass('active');
  });
})(jQuery);
