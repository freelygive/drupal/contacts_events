<?php

namespace Drupal\contacts_events_segments;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Route provider for Contacts Events Segments entity.
 */
class SegmentHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('collection')) {
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $label */
      $label = $entity_type->getCollectionLabel();
      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route
        ->addDefaults([
          '_entity_list' => $entity_type->id(),
          '_title' => $label->getUntranslatedString(),
          '_title_arguments' => $label->getArguments(),
          '_title_context' => $label->getOption('context'),
        ])
        ->setRequirement('_contacts_events_segments', 'TRUE')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          'contacts_event' => ['type' => 'entity:contacts_event'],
        ]);

      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    return parent::getEditFormRoute($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getAddFormRoute($entity_type)) {
      $requirements = $route->getRequirements();
      unset($requirements['_entity_create_access']);
      $route->setRequirements($requirements);
      $route->setRequirement('_contacts_events_segments', 'TRUE');
      $route->setOption('parameters', [
        'contacts_event' => ['type' => 'entity:contacts_event'],
      ]);
    }
    return $route;
  }

}
