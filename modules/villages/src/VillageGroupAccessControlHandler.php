<?php

namespace Drupal\contacts_events_villages;

use Drupal\contacts_events\Access\EventAccessTrait;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Camping village group entity.
 *
 * @see \Drupal\contacts_events_villages\Entity\VillageGroup.
 */
class VillageGroupAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  use EventAccessTrait;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return (new static($entity_type))
      ->setRouteMatch($container->get('current_route_match'))
      ->setEventStorage($container->get('entity_type.manager')->getStorage('contacts_event'));
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroupInterface $entity */
    $event = $entity->getEvent();

    if (!$this->checkEvent($event, $entity->bundle())) {
      return AccessResult::forbidden()
        ->addCacheableDependency($event);
    }

    switch ($operation) {
      case 'view label':
        return AccessResult::allowed();

      case 'view':
      case 'update':
      case 'delete':
        // Check can manage villages and also update event.
        return AccessResult::allowedIfHasPermission($account, 'manage c_events_village_group entities')
          ->andIf(AccessResult::allowedIf($event->access('update', $account)));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($event = $this->getEventFromContextOrRoute($context)) {
      if (!$this->checkEvent($event, $entity_bundle)) {
        return AccessResult::forbidden()
          ->addCacheableDependency($event);
      }
    }

    // Parent class checks access against entity admin permission.
    return parent::checkCreateAccess($account, $context, $entity_bundle);
  }

  /**
   * Check whether accommodation and village groups are enabled for the event.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event the village group is for.
   * @param string $bundle
   *   The bundle of village group we are checking.
   *
   * @return bool
   *   Whether accommodation is enabled.
   */
  protected function checkEvent(EventInterface $event, $bundle) {
    if (!$event->hasField('accommodation_types')) {
      return FALSE;
    }

    // Check that camping accommodation is enabled.
    $accommodation_types = array_map(function ($item) {
      return $item['target_id'];
    }, $event->get('accommodation_types')->getValue());
    if (!in_array('camping', $accommodation_types)) {
      return FALSE;
    }

    if (!$event->hasField('village_group_types')) {
      return FALSE;
    }

    // Check that village group bundle is enabled.
    $village_group_types = array_map(function ($item) {
      return $item['value'];
    }, $event->get('village_group_types')->getValue());
    return in_array($bundle, $village_group_types);
  }

}
