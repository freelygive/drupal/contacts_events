<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Calculates which village IDs the group is allocated to.
 *
 * @package Drupal\contacts_events_villages\Entity
 */
class AllocatedVillagesItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $entity */
    $entity = $this->getEntity();

    $query = \Drupal::database()->select('commerce_order', 'o');
    $query->leftJoin('commerce_order__village', 'v', 'v.entity_id = o.order_id');
    $query->join('commerce_order__village_group', 'vg', 'vg.entity_id = o.order_id');
    $query->condition('o.state', 'draft', '<>');
    $query->condition('vg.village_group_target_id', $entity->id());
    $query->addField('v', 'village_target_id', 'village_id');
    $query->distinct();

    $results = $query->execute()->fetchCol();

    $i = 0;
    foreach ($results as $village_id) {
      if ($village_id == NULL) {
        // If we have an unallocated booking, add a fake village ID of 0
        // which represents "has unallocated bookings".
        $village_id = 0;
      }

      $this->list[$i] = $this->createItem($i, $village_id);
      $i++;
    }
  }

  /**
   * Reset the computed value.
   */
  public function resetValue() {
    $this->valueComputed = FALSE;
  }

}
