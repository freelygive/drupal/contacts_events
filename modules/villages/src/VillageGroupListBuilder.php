<?php

namespace Drupal\contacts_events_villages;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Camping village group entities.
 *
 * @ingroup contacts_events_villages
 */
class VillageGroupListBuilder extends EntityListBuilder {

  /**
   * The village group type plugin manager.
   *
   * @var \Drupal\contacts_events_villages\Plugin\VillageGroupTypeManager
   */
  protected $pluginManager;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * A cache of the group type labels.
   *
   * @var array
   */
  protected $groupTypeLabels;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $self = parent::createInstance($container, $entity_type);
    $self->pluginManager = $container->get('plugin.manager.c_events_village_group_type');
    $self->routeMatch = $container->get('current_route_match');
    return $self;
  }

  /**
   * Get the label for the group type.
   *
   * @param string $type
   *   The group type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The group type label.
   */
  protected function getGroupTypeLabel(string $type) : TranslatableMarkup {
    if (!isset($this->groupTypeLabels)) {
      foreach ($this->pluginManager->getDefinitions() as $id => $def) {
        $this->groupTypeLabels[$id] = $def['label'];
      }
    }

    return $this->groupTypeLabels[$type];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'));
    $query->accessCheck();

    // Filter by current event.
    if ($event = $this->routeMatch->getParameter('contacts_event')) {
      $query->condition('event', $event->id());
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['type'] = $this->t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $entity */
    $row['id'] = $entity->id();
    $row['name'] = $entity->toLink();
    $row['type'] = $this->getGroupTypeLabel($entity->bundle());
    return $row + parent::buildRow($entity);
  }

}
