<?php

namespace Drupal\contacts_events_villages\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form controller for Camping village group edit forms.
 *
 * @ingroup contacts_events_villages
 */
class VillageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    /** @var \Drupal\contacts_events_villages\Entity\Village $entity */
    $entity = parent::getEntityFromRouteMatch($route_match, $entity_type_id);

    // Pre-populate event ID from URL.
    if ($entity->isNew()) {
      $event = $route_match->getParameter('contacts_event');
      $entity->setEvent($event);
    }
    elseif ($entity->get('event')->isEmpty()) {
      throw new NotFoundHttpException('Invalid event.');
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addMessage($this->t('Saved the %label village.', [
      '%label' => $this->entity->label(),
    ]));
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
