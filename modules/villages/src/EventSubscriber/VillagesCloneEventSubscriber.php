<?php

namespace Drupal\contacts_events_villages\EventSubscriber;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Event\CloneEvent;
use Drupal\contacts_events\EventSubscriber\CloneEventDependentEntitySubscriberBase;
use Drupal\contacts_events_teams\EventSubscriber\TeamsCloneReferenceTrait;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Villages clone event subscriber.
 */
class VillagesCloneEventSubscriber extends CloneEventDependentEntitySubscriberBase {

  use TeamsCloneReferenceTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Use a low priority so we run after team cloning.
      CloneEvent::class => ['onClone', -50],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onClone(CloneEvent $event): void {
    $event->addOperation(
      'contacts_events_villages.clone_event_subscriber',
      'cloneVillagesOperation',
      new TranslatableMarkup('Cloning villages'),
    );
    $event->addOperation(
      'contacts_events_villages.clone_event_subscriber',
      'cloneVillageGroupsOperation',
      new TranslatableMarkup('Cloning village groups'),
    );
    $event->addOperation(
      'contacts_events_villages.clone_event_subscriber',
      'updateVillageHostTeam',
      new TranslatableMarkup('Update village host team'),
    );
  }

  /**
   * Clone operation to clone villages.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The new event.
   * @param \Drupal\contacts_events\Entity\EventInterface $source
   *   The source event.
   * @param array $sandbox
   *   A sandbox for storing data that will be preserved between calls to this
   *   operation.
   *
   * @return int
   *   The progress percentage between 0 (not started) and 100 (complete).
   */
  public function cloneVillagesOperation(EventInterface $event, EventInterface $source, array &$sandbox): int {
    return $this->cloneDependantEntityOperation('c_events_village', $event, $source, $sandbox, 'event');
  }

  /**
   * Clone operation to clone village groups.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The new event.
   * @param \Drupal\contacts_events\Entity\EventInterface $source
   *   The source event.
   * @param array $sandbox
   *   A sandbox for storing data that will be preserved between calls to this
   *   operation.
   *
   * @return int
   *   The progress percentage between 0 (not started) and 100 (complete).
   */
  public function cloneVillageGroupsOperation(EventInterface $event, EventInterface $source, array &$sandbox): int {
    return $this->cloneDependantEntityOperation('c_events_village_group', $event, $source, $sandbox, 'event');
  }

  /**
   * Clone operation to update the village host team on the clone.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The new event.
   * @param \Drupal\contacts_events\Entity\EventInterface $source
   *   The source event.
   */
  public function updateVillageHostTeam(EventInterface $event, EventInterface $source): void {
    if (!$source->hasField('village_host_teams')) {
      return;
    }

    $team_ids = $this->findClonedTeams($source->get('village_host_teams')->referencedEntities(), $event->id());

    // Set the teams on the event. We don't need to save, as storing progress
    // will update the event.
    $event->set('village_host_teams', $team_ids);
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseQuery(string $entity_type_id, int $event_id, string $event_field): QueryInterface {
    $query = parent::getBaseQuery($entity_type_id, $event_id, $event_field);

    // For village groups, only clone the 'other' type.
    if ($entity_type_id === 'c_events_village_group') {
      // @todo Perhaps move this into the annotation of the type?
      $query->condition('type', 'other');
    }

    return $query;
  }

}
