<?php

namespace Drupal\contacts_events_villages\Plugin\views\filter;

use Drupal\commerce\Plugin\views\filter\EntityBundle;
use Drupal\views\Plugin\views\HandlerBase;

/**
 * Filters by Village Group bundle.
 *
 * Can be configured to hide the exposed form when there's only one possible
 * bundle.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("village_group_bundle")
 */
class VillageGroupBundle extends EntityBundle {

  /**
   * Gets the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  protected function getEntityTypeManager() {
    if (!$this->entityTypeManager) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;

  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $types = $this->bundleInfoService->getBundleInfo($this->entityTypeId);
      // When the filter is exposed, filter out bundles that the user is
      // not allowed to see. Workaround for core issue #3099068.
      $storage = $this->getEntityTypeManager()->getStorage($this->entityTypeId);
      $event_storage = $this->getEntityTypeManager()->getStorage('contacts_event');
      foreach ($types as $type => $info) {
        if ($this->isExposed()) {
          // Create a stub entity to check access, with a stub event entity
          // with the minimal values for access checking.
          $stub_entity = $storage->create([
            $this->entityType->getKey('bundle') => $type,
            'event' => $event_storage->create([
              'type' => 'default',
              'accommodation_types' => ['camping'],
              'village_group_types' => [$type],
            ]),
          ]);
          if (!$stub_entity->access('view')) {
            unset($types[$type]);
          }
        }
      }

      $this->valueTitle = $this->t('@entity types', ['@entity' => $this->entityType->getLabel()]);
      $options = [];
      foreach ($types as $type => $info) {
        $options[$type] = $info['label'];
      }

      asort($options);
      $this->valueOptions = $options;
    }

    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   *
   * Same as EntityBundle::calculateDependencies() but without
   * get_parent_class($this) which causes issues when the class is extended.
   */
  public function calculateDependencies() {
    $dependencies = HandlerBase::calculateDependencies();

    $bundle_entity_type = $this->entityType->getBundleEntityType();
    if ($bundle_entity_type) {
      $bundle_entity_storage = $this->getEntityTypeManager()->getStorage($bundle_entity_type);

      foreach (array_keys($this->value) as $bundle) {
        if ($bundle_entity = $bundle_entity_storage->load($bundle)) {
          $dependencies[$bundle_entity->getConfigDependencyKey()][] = $bundle_entity->getConfigDependencyName();
        }
      }
    }

    return $dependencies;
  }

}
