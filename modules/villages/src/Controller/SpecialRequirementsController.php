<?php

namespace Drupal\contacts_events_villages\Controller;

use Drupal\contacts_events\Entity\Event;
use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for the manage special requirements page.
 *
 * @package Drupal\contacts_events_villages\Controller
 */
class SpecialRequirementsController extends ControllerBase {

  /**
   * Special requirements page.
   *
   * @param \Drupal\contacts_events\Entity\Event $contacts_event
   *   The event.
   *
   * @return array
   *   Render array.
   */
  public function requirements(Event $contacts_event) {
    $build = [];

    $build['view'] = [
      '#type' => 'view',
      '#name' => 'village_special_requirements',
      '#display_id' => 'embed_1',
      '#arguments' => [$contacts_event->id()],
    ];

    return $build;
  }

}
