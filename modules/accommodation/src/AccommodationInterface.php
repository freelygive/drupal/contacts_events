<?php

namespace Drupal\contacts_events_accommodation;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an accommodation entity type.
 */
interface AccommodationInterface extends ContentEntityInterface, PurchasableEntityInterface {

  /**
   * Get the accommodation description.
   *
   * @return string
   *   The description.
   */
  public function getDescription() : ?string;

  /**
   * Get the ID of the event the accommodation is for.
   *
   * @return int
   *   The event ID.
   */
  public function getEventId() : int;

  /**
   * Get the event the accommodation is for.
   *
   * @return \Drupal\contacts_events\Entity\EventInterface
   *   The event.
   */
  public function getEvent() : EventInterface;

  /**
   * Get how many of this accommodation are available for the event.
   *
   * @return int|null
   *   The available amount, or NULL if unlimited.
   */
  public function getAvailable() : ?int;

  /**
   * Get the minimum number of delegates this accommodation is suitable for.
   *
   * @return int|null
   *   The minimum number, or NULL if not applicable.
   */
  public function getMinDelegates() : ?int;

  /**
   * Get the maximum number of delegates this accommodation is suitable for.
   *
   * @return int|null
   *   The maximum number, or NULL if not applicable.
   */
  public function getMaxDelegates() : ?int;

  /**
   * Get a textual representation of the delegate numbers.
   *
   * @return \Drupal\Component\Render\FormattableMarkup|null
   *   The delegate range as text, or NULL if not applicable.
   */
  public function getDelegatesText() : ?FormattableMarkup;

}
