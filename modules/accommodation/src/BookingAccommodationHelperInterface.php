<?php

namespace Drupal\contacts_events_accommodation;

use Drupal\contacts_events\PriceCalculator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Helper for working with accommodation for a booking.
 */
interface BookingAccommodationHelperInterface {

  /**
   * Construct the booking accommodation helper.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   The order items field of an order.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\contacts_events\PriceCalculator $price_calculator
   *   The price calculator service.
   */
  public function __construct(EntityReferenceFieldItemListInterface $items, EntityTypeManagerInterface $entity_type_manager, PriceCalculator $price_calculator);

  /**
   * Get the number of confirmed delegates.
   *
   * @return int
   *   The number of confirmed delegates.
   */
  public function getConfirmedDelegates(): int;

  /**
   * Get the total number of delegates.
   *
   * @return int
   *   The total number of delegates.
   */
  public function getTotalDelegates(): int;

  /**
   * Get the confirmed accommodation of the given type on the booking.
   *
   * @param int $id
   *   The accommodation ID.
   *
   * @return int
   *   The confirmed count.
   */
  public function getConfirmedAccommodation(int $id): int;

  /**
   * Get the total accommodation of the given type on the booking.
   *
   * @param int $id
   *   The accommodation ID.
   *
   * @return int
   *   The total count.
   */
  public function getTotalAccommodation(int $id): int;

  /**
   * Get the accommodation quantities for all accommodation on the booking.
   *
   * @return array
   *   An array of accommodation. Keys are accommodation ID, values are arrays
   *   containing:
   *   - total: The total quantity of that accommodation.
   *   - confirmed: The confirmed quantity of that accommodation.
   */
  public function getAllAccommodation(): array;

  /**
   * Get the maximum allowed of a given accommodation type.
   *
   * This will always be at least the confirmed count.
   *
   * @param \Drupal\contacts_events_accommodation\AccommodationInterface $accommodation
   *   The accommodation type.
   *
   * @return int|null
   *   The maximum number, or NULL if there is no max.
   */
  public function getMaxAllowedAccommodation(AccommodationInterface $accommodation): ?int;

  /**
   * Update the order items with unconfirmed items.
   *
   * @param array $quantities
   *   An array of accommodation ID to desired quantity.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface[]
   *   The modified order items, keyed by accommodation ID.
   */
  public function updateUnconfirmedItems(array $quantities);

}
