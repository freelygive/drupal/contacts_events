<?php

namespace Drupal\contacts_events_accommodation\Entity;

use Drupal\commerce_price\Price;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events_accommodation\AccommodationInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Defines the accommodation entity class.
 *
 * @ContentEntityType(
 *   id = "c_events_accommodation",
 *   label = @Translation("Accommodation"),
 *   label_collection = @Translation("Accommodation"),
 *   bundle_label = @Translation("Accommodation type"),
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_events_accommodation\AccommodationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\contacts_events_accommodation\Form\AccommodationForm",
 *       "edit" = "Drupal\contacts_events_accommodation\Form\AccommodationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "access" = "Drupal\contacts_events_accommodation\AccommodationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_accommodation\AccommodationHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "c_events_accommodation",
 *   data_table = "c_events_accommodation_field_data",
 *   translatable = TRUE,
 *   admin_permission = "manage contacts events accommodation",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/event/{contacts_event}/accommodation/add/{c_events_accommodation_type}",
 *     "canonical" = "/event/{contacts_event}/accommodation/{c_events_accommodation}",
 *     "delete-form" = "/event/{contacts_event}/accommodation/{c_events_accommodation}/delete",
 *     "collection" = "/event/{contacts_event}/accommodation"
 *   },
 *   bundle_entity_type = "c_events_accommodation_type",
 *   field_ui_base_route = "entity.c_events_accommodation_type.edit_form"
 * )
 */
class Accommodation extends ContentEntityBase implements AccommodationInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventId(): int {
    return $this->get('event')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvent(): EventInterface {
    return $this->get('event')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailable(): ?int {
    return $this->get('available')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMinDelegates(): ?int {
    return $this->get('delegates_min')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxDelegates(): ?int {
    return $this->get('delegates_max')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDelegatesText(): ?FormattableMarkup {
    $delegates_min = $this->get('delegates_min')->value;
    if (!isset($delegates_min)) {
      return NULL;
    }

    $delegates_max = $this->get('delegates_max')->value;
    if ($delegates_min == $delegates_max) {
      return new FormattableMarkup('@count', [
        '@count' => $delegates_min,
      ]);
    }

    return new FormattableMarkup('@min - @max', [
      '@min' => $delegates_min,
      '@max' => $delegates_max,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $params = parent::urlRouteParameters($rel);

    // Add the event ID to the parameters.
    $params['contacts_event'] = $this->get('event')->target_id;

    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function getStores() {
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_store');
    $id = \Drupal::config('contacts_events.booking_settings')->get('store_id');
    return $storage->loadMultiple([$id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItemTypeId() {
    return 'ce_accom_' . $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItemTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice() {
    // Let the calculator handle this.
    return new Price(0, $this->getDefaultCurrencyCode());
  }

  /**
   * Get the default currency code.
   *
   * @return string
   *   The default currency code.
   */
  protected function getDefaultCurrencyCode() {
    $stores = $this->getStores();
    $store = reset($stores);
    return $store->getDefaultCurrencyCode();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(new TranslatableMarkup('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Event'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('target_type', 'contacts_event')
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Weight'))
      ->setRequired(TRUE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'label' => 'above',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDescription(new TranslatableMarkup('A description of the accommodation.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['public'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Public'))
      ->setDescription(new TranslatableMarkup('Public accommodation is bookable by the public.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'label' => 'above',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['available'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Available'))
      ->setDescription(new TranslatableMarkup('How many of this accommodation are available. Leave empty for unlimited. Only used for reporting.'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'label' => 'above',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['delegates_min'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Minimum delegates'))
      ->setDescription(new TranslatableMarkup('The minimum number of delegates this accommodation is suitable for. Booking managers will not be able to select this accommodation type if they do not have at least this many delegates. Leave empty to exclude from delegate count validation.'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'label' => 'above',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['delegates_max'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Maximum delegates'))
      ->setDescription(new TranslatableMarkup('The maximum number of delegates this accommodation is suitable for. Booking managers will be warned if their total maximum accommodation selection is not enough for their delegates.'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'label' => 'above',
        'weight' => 26,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number',
        'weight' => 26,
      ])
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    if ($bundle == 'camping') {
      $fields['pitch_size'] = BundleFieldDefinition::create('decimal')
        ->setLabel(new TranslatableMarkup('Pitch size'))
        ->setDescription(new TranslatableMarkup('The pitch size this accommodation occupies.'))
        ->setDisplayOptions('form', [
          'type' => 'number',
          'label' => 'above',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'type' => 'number_decimal',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('view', FALSE);
    }

    return $fields;
  }

}
