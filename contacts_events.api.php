<?php

/**
 * @file
 * Describes hooks for Contacts Events.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow denying of bookings with a reason displayed to the user.
 *
 * The first reason provided by an implementation will be displayed to the user.
 *
 * @param \Drupal\contacts_events\Entity\EventInterface $event
 *   The event we are attempting to book for.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account the booking is for.
 *
 * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
 *   A denial reason, or NULL to not deny.
 */
function hook_contacts_events_deny_booking(\Drupal\contacts_events\Entity\EventInterface $event, \Drupal\Core\Session\AccountInterface $account) {
  // Don't allow administrators to book for events.
  if (in_array('administrator', $account->getRoles())) {
    return new TranslatableMarkup('Administrators are now allowed to book on events.');
  }

  return NULL;
}

/**
 * Allow modification of the ticket form.
 *
 * Using a custom hook instead of hook_inline_entity_form_entity_form_alter as
 * it runs in the base entityForm method, so won't pick up the changes made by
 * addAjaxPrice.
 */
function hook_contacts_events_ticket_form_alter(array &$form, \Drupal\Core\Form\FormStateInterface $form_state, \Drupal\contacts_events\Entity\Ticket $ticket, string $display_mode) {
  // Register a new field to be updated on ticket calculation ajax updates.
  $form['new_field']['#id'] = 'new-field-ajax-wrapper';
  $form['price_update']['#element']->registerElementToUpdate($form['new_field']);
}

/**
 * Add to or modify the ticket summary page.
 *
 * @param array $build
 *   The build array.
 * @param \Drupal\contacts_events\Entity\EventInterface $event
 *   The event entity.
 *
 * @see \Drupal\contacts_events\Controller\TicketsController::summary()
 */
function hook_contacts_events_ticket_summary_alter(array &$build, \Drupal\contacts_events\Entity\EventInterface $event) {
  $build['teams'] = ['#type' => 'fieldset'];
  $build['teams']['delegates'] = [
    '#type' => 'item',
    '#title' => new TranslatableMarkup('Delegates'),
    '#markup' => 10,
    '#description' => new TranslatableMarkup('Non team event delegates.'),
  ];
}

/**
 * Provides custom evaluation logic for an event class.
 *
 * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
 *   Order item.
 * @param \Drupal\contacts_events\Entity\EventClassInterface $event_class
 *   Event class.
 *
 * @return bool|null
 *   If a bool is set, this OVERRIDES the result of the default age check, so
 *   age checking must be part of your hook implementation.
 */
function hook_contacts_events_class_evaluate(\Drupal\commerce_order\Entity\OrderItemInterface $order_item, \Drupal\contacts_events\Entity\EventClassInterface $event_class): ?bool {
  if ($event_class->get('type') == 'contacts_ticket' && $event_class->id() == 'some_event_class') {
    if ($order_item->get('purchased_entity')->get('some_custom_value_to_compare')->getString()) {
      return TRUE;
    }
  }

  return NULL;
}

/**
 * @} End of "addtogroup hooks".
 */
