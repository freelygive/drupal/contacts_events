<?php

namespace Drupal\contacts_events;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Helper for finding bookings for users.
 */
class UserBookingsHelper {

  /**
   * Storage class for bookings.
   *
   * @var \Drupal\commerce_order\OrderStorage
   */
  protected $orderStorage;

  /**
   * Storage class for bookings.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $ticketStorage;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Construct the user bookings helper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, CacheBackendInterface $cache) {
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
    $this->ticketStorage = $entity_type_manager->getStorage('contacts_ticket');
    $this->currentUser = $current_user;
    $this->cache = $cache;
  }

  /**
   * Find a booking for an event and booking manager.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user. If not provided, we use the current user.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order, if available.
   */
  public function findBookingForManager(EventInterface $event, AccountInterface $account = NULL): ?OrderInterface {
    // If we didn't get an account, use the current user.
    if (!$account) {
      $account = $this->currentUser;
    }

    $cid = "booking_manager:{$account->id()}";
    $cache = $this->cache->get($cid);
    if ($cache) {
      $order_id = $cache->data;
    }
    else {
      // Query booking for this event for this user.
      $query = $this->orderStorage->getQuery();
      $query->accessCheck(FALSE);
      $query->condition('uid', $account->id());
      $query->condition('type', 'contacts_booking');
      $query->condition('event', $event->id());
      $result = $query->execute();

      $order_id = NULL;
      if (!empty($result)) {
        $order_id = reset($result);
      }

      $tag = "user_bookings:order:{$account->id()}";
      $this->cache->set($cid, $order_id, CacheBackendInterface::CACHE_PERMANENT, [$tag]);
    }

    if ($order_id) {
      return $this->orderStorage->load($order_id);
    }

    return NULL;
  }

  /**
   * Find a booking for an event and ticket holder.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user. If not provided, we use the current user.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order, if available.
   */
  public function findBookingForTicketHolder(EventInterface $event, AccountInterface $account = NULL): ?OrderInterface {
    // If we didn't get an account, use the current user.
    if (!$account) {
      $account = $this->currentUser;
    }

    $cid = "ticket_holder:{$account->id()}";
    $cache = $this->cache->get($cid);
    if ($cache) {
      $order_id = $cache->data;
    }
    else {
      // Query ticket for this event for this user.
      $query = $this->ticketStorage->getQuery();
      $query->accessCheck(FALSE);
      $query->condition('contact', $account->id());
      $query->condition('event', $event->id());
      $result = $query->execute();

      $order_id = NULL;
      if (!empty($result)) {
        $ticket_id = reset($result);
        $order = $this->ticketStorage->load($ticket_id)->getBooking();
        if ($order) {
          $order_id = $order->id();
        }
      }
      $tag = "user_bookings:ticket:{$account->id()}";
      $this->cache->set($cid, $order_id, CacheBackendInterface::CACHE_PERMANENT, [$tag]);
    }

    if ($order_id) {
      return $this->orderStorage->load($order_id);
    }

    return NULL;
  }

}
