<?php

namespace Drupal\contacts_events;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\CurrencyFormatter;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Utility\Token;

/**
 * Sends admin payment confirmation emails.
 *
 * @package Drupal\contacts_events
 */
class AdminPaymentEmailService {

  /**
   * The email key.
   */
  const MAIL_KEY = 'admin_payment_confirmation';

  /**
   * The admin payment conirmation email config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The mail plugin manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The price formatter service.
   *
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $priceFormatter;

  /**
   * TeamEmailSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Utility\Token $token
   *   Token replacement service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Drupal\commerce_price\CurrencyFormatter $formatter
   *   Price formatter service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Token $token, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager, CurrencyFormatter $formatter) {
    $this->config = $config_factory->get('contacts_events.booking_settings');
    $this->token = $token;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->priceFormatter = $formatter;
  }

  /**
   * Sends an email to the payer.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment that was created.
   * @param string $to
   *   The email address to send the email to.
   */
  public function sendToPayer(PaymentInterface $payment, string $to) : void {
    $email_config = $this->config->get('admin_payment_confirmation');

    // Get our parts of the email.
    $params['subject'] = $email_config['subject'];
    $params['body'] = $email_config['email'];
    $params['from'] = $payment->getOrder()->getStore()->getEmail();
    $params['payment'] = $payment;

    /** @var \Drupal\user\UserInterface $user */
    $user = $payment->getOrder()->get('uid')->entity;
    if ($user) {
      $langcode = $user->getPreferredLangcode();
    }
    else {
      $langcode = $this->languageManager->getDefaultLanguage()->getId();
    }

    $this->mailManager->mail('contacts_events', self::MAIL_KEY, $to, $langcode, $params);
  }

  /**
   * Build the notification email.
   *
   * @param array $message
   *   The message.
   * @param array $params
   *   The params.
   *
   * @see \hook_mail()
   */
  public function buildMail(array &$message, array $params) : void {
    $subject = $params['subject'];
    $body = $params['body'];

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $params['payment'];
    $order = $payment->getOrder();
    $event = $order->get('event')->entity;
    $tokens = [
      'commerce_payment' => $payment,
      'commerce_order' => $order,
      'contacts_event' => $event,
      'user' => $order->getCustomer(),
    ];
    $subject = $this->token->replace($subject, $tokens);

    // Until https://www.drupal.org/project/commerce/issues/3078918 is fixed,
    // replace the 'commerce_payment:amount:formatted' token manually.
    $formatted_payment = $this->priceFormatter->format($payment->getAmount()->getNumber(), $payment->getAmount()->getCurrencyCode());
    $body = str_replace('[commerce_payment:amount:formatted]', $formatted_payment, $body);
    if (is_array($body)) {
      $body['value'] = $this->token->replace($body['value'], $tokens);
    }
    else {
      $body = $this->token->replace($body, $tokens);
    }

    if (is_array($body)) {
      $renderable = [
        '#type' => 'processed_text',
        '#format' => $body['format'],
        '#text' => $body['value'],
      ];
      $body = \Drupal::service('renderer')->renderPlain($renderable);

      // Indicate the email is in HTML.
      $message['headers']['Content-Type'] = 'text/html; charset=UTF-8';
    }

    $message['subject'] = $subject;
    $message['body'][] = $body;

    if ($params['from']) {
      $message['headers']['From'] = $params['from'];
    }
  }

}
