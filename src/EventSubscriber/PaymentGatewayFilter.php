<?php

namespace Drupal\contacts_events\EventSubscriber;

use Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent;
use Drupal\commerce_payment\Event\PaymentEvents;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ensures refund payments are only available in the back end.
 */
class PaymentGatewayFilter implements EventSubscriberInterface {

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * PaymentGatewayFilter constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Current route.
   */
  public function __construct(RouteMatchInterface $routeMatch) {
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[PaymentEvents::FILTER_PAYMENT_GATEWAYS][] = ['filter'];
    return $events;
  }

  /**
   * Filters the available payment gateways.
   *
   * @param \Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent $event
   *   The event instance containing payment gateways.
   */
  public function filter(FilterPaymentGatewaysEvent $event) {
    // Make sure refund payments are only available when managing payments via
    // the back end add payment form.
    if ($this->routeMatch->getRouteName() !== 'entity.commerce_payment.add_form') {
      $gateways = $event->getPaymentGateways();
      foreach ($gateways as $id => $gateway) {
        if ($gateway->getPluginId() == 'manual_refund') {
          unset($gateways[$id]);
        }
      }
      $event->setPaymentGateways($gateways);
    }
  }

}
