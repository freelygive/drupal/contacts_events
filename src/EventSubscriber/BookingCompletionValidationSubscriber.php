<?php

namespace Drupal\contacts_events\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Event\BookingCompletionValidationEvent;
use Drupal\contacts_events\SupervisionHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber for booking completion validation.
 */
class BookingCompletionValidationSubscriber implements EventSubscriberInterface {

  /**
   * The supervision helper service.
   *
   * @var \Drupal\contacts_events\SupervisionHelper
   */
  protected SupervisionHelper $supervisionHelper;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Construct the subscriber.
   *
   * @param \Drupal\contacts_events\SupervisionHelper $supervision_helper
   *   The supervision helper service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(SupervisionHelper $supervision_helper, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->supervisionHelper = $supervision_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BookingCompletionValidationEvent::NAME => [
        ['validateBookingsStatus', -99],
        ['validateSupervision'],
        ['validateCapacity'],
      ],
    ];
  }

  /**
   * Validate that completion is allowed for this event.
   *
   * @param \Drupal\contacts_events\Event\BookingCompletionValidationEvent $validation_event
   *   Validation event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateBookingsStatus(BookingCompletionValidationEvent $validation_event): void {
    // Check the booking status of the event.
    $order = $validation_event->getOrder();
    /** @var \Drupal\contacts_events\Entity\EventInterface $event */
    $event = $order->get('event')->entity;

    // If booking is open, there's nothing to do.
    if ($event->isBookingOpen()) {
      return;
    }

    // Not enabled shouldn't even allow other validation to take place.
    if (!$event->isBookingEnabled()) {
      $validation_event->addError(new TranslatableMarkup('This event does not allow bookings.'));
      $validation_event->stopPropagation();
      return;
    }

    // Disallow confirming if there are no items.
    $items = $order->getItems();
    if (count($items) === 0) {
      $validation_event->addError(new TranslatableMarkup('This booking is empty.'));
      $validation_event->stopPropagation();
      return;
    }

    // Check whether there are unconfirmed items on the booking and get the
    // total for any confirmed items.
    $has_unconfirmed = $this->hasUnconfirmed($order, TRUE);

    // If there are unconfirmed items we show a soft error to allow users with
    // the manage permission to continue.
    if ($has_unconfirmed['has_unconfirmed']) {

      // If there is an outstanding balance on the confirmed items, we will
      // show a slightly different message.
      $paid = $order->getTotalPaid();
      if ($has_unconfirmed['confirmed_total'] && $has_unconfirmed['confirmed_total']->isPositive() && (!$paid || $paid->lessThan($has_unconfirmed['confirmed_total']))) {
        $validation_event->addSoftError(new TranslatableMarkup('You are now unable to make any changes to this booking. To pay your outstanding balance, please remove any attempted changes to this booking.'));
      }
      // Otherwise we can simplify the message.
      else {
        $validation_event->addSoftError(new TranslatableMarkup('You are now unable to make any changes to this booking.'));
      }
    }
  }

  /**
   * Validate supervision ratio.
   *
   * @param \Drupal\contacts_events\Event\BookingCompletionValidationEvent $event
   *   The validation event.
   */
  public function validateSupervision(BookingCompletionValidationEvent $event) {
    // Check that event is configured for child supervision validation.
    $ratio = $this->supervisionHelper->getRatio($event->getOrder());
    if (!$ratio) {
      return;
    }

    // Get the booking helper to calculate current ratios.
    $booking_helper = $this->supervisionHelper->getBookingHelper($event->getOrder()->get('order_items'));
    $adults_total = $booking_helper->getAdultDelegates();
    $non_adult_total = $booking_helper->getNonAdultDelegates();
    $result = $this->supervisionHelper->validateSupervision($adults_total, $non_adult_total, $ratio);

    switch ($result) {
      case SupervisionHelper::VALIDATE_OK:
      case SupervisionHelper::VALIDATE_UNNECESSARY:
        // No error, so nothing to do.
        break;

      case SupervisionHelper::VALIDATE_TOO_FEW:
        $error = new TranslatableMarkup('It looks like you have not booked enough adults to supervise the @count you are booking for.', [
          '@count' => new PluralTranslatableMarkup($non_adult_total, 'under 18', '@count under 18s'),
        ]);
        break;

      case SupervisionHelper::VALIDATE_NONE:
        $error = new TranslatableMarkup('It looks like you have not booked any adults for this booking. At least one delegate must be over 18 at the time of the event.');
        break;

      default:
        $error = new TranslatableMarkup('Sorry, we encountered an unknown error.');
        break;
    }

    if (isset($error)) {
      $event->addSoftError($error);
    }
  }

  /**
   * Checks that capacity is checkable and has not been reached.
   *
   * @param \Drupal\contacts_events\Event\BookingCompletionValidationEvent $validation_event
   *   The validation event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateCapacity(BookingCompletionValidationEvent $validation_event): void {
    $config = $this->configFactory->get('contacts_events.booking_settings');

    if ($config->get('capacity_hard_limit')) {
      $order = $validation_event->getOrder();

      // Check whether there are unconfirmed items on the booking and get the
      // total for any confirmed items.
      $has_unconfirmed = $this->hasUnconfirmed($order);

      // If there are unconfirmed items we show a soft error to allow users with
      // the manage permission to continue.
      if ($has_unconfirmed['has_unconfirmed']) {

        /** @var \Drupal\contacts_events\Entity\EventInterface $event */
        $event = $order->get('event')->entity;

        // If the event is at capacity, confirmation should be blocked.
        if ($event->hasField('capacity') && !$event->get('capacity')->isEmpty()) {
          if ($this->getConfirmedTicketsCount($event) >= $event->get('capacity')->getString()) {
            $validation_event->addSoftError(new TranslatableMarkup('This event is now full, if you would like to attend please contact a member of staff'));
          }
        }
      }
    }
  }

  /**
   * Helper to load a count of all confirmed tickets for an event.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   An event to get the count for.
   *
   * @return array|int
   *   The count of confirmed tickets.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getConfirmedTicketsCount(EventInterface $event) {
    $query = $this->entityTypeManager->getStorage('contacts_ticket')->getQuery();
    $query->condition('event', $event->id());
    $query->condition('order_item.entity:commerce_order_item.state', [
      'confirmed',
      'paid_in_full',
    ], 'IN');

    return $query->accessCheck(FALSE)->count()->execute();
  }

  /**
   * Helper to see if there are unconfirmed tickets on an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check.
   * @param bool $return_confirmed_total
   *   If TRUE, the total value of confirmed tickets will be returned.
   *
   * @return array
   *   ['has_unconfirmed' => (bool), 'confirmed_total' => FALSE|CommercePrice]
   */
  private function hasUnconfirmed(OrderInterface $order, bool $return_confirmed_total = FALSE): array {
    $return = [
      'has_unconfirmed' => FALSE,
      'confirmed_total' => FALSE,
    ];

    $items = $order->getItems();

    if ($return_confirmed_total) {

      // Check whether there are unconfirmed items on the booking and get the
      // total for any confirmed items.
      foreach ($items as $item) {
        if ($item->hasField('state') && $item->get('state')->value == 'pending') {
          $return['has_unconfirmed'] = TRUE;
        }
        elseif ($item_total = $item->getAdjustedTotalPrice()) {
          $return['confirmed_total'] = $return['confirmed_total'] !== FALSE ? $return['confirmed_total']->add($item_total) : $item_total;
        }
      }
    }
    else {

      // If we don't need to collect the confirmed total amount, we can exit the
      // function as soon as we know there is one unconfirmed ticket.
      $key = 0;

      while (array_key_exists($key, $items) && $return['has_unconfirmed'] === FALSE) {
        if ($items[$key]->hasField('state') && $items[$key]->get('state')->value == 'pending') {
          $return['has_unconfirmed'] = TRUE;
        }

        $key++;
      }
    }

    return $return;
  }

}
