<?php

namespace Drupal\contacts_events\Access;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks if the order in the route is a booking.
 */
class BookingAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The order being accessed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(OrderInterface $commerce_order) {
    return AccessResult::allowedIf($commerce_order->bundle() == 'contacts_booking')
      ->addCacheableDependency($commerce_order);
  }

}
