<?php

namespace Drupal\contacts_events\PluginForm;

use Drupal\commerce_partial_payments\Form\TrackingElementTrait;
use Drupal\commerce_partial_payments\OrderItemTrackingInterface;
use Drupal\commerce_payment\PluginForm\PaymentReceiveForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The manual refund receive form.
 */
class ManualRefundPaymentReceiveForm extends PaymentReceiveForm implements ContainerInjectionInterface {

  use TrackingElementTrait;
  use MessengerTrait;

  /**
   * The order item payment tracker.
   *
   * @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface
   */
  protected $paymentTracking;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $class = new static(
      $container->get('commerce_partial_payments.order_item_tracking')
    );
    $class->setStringTranslation($container->get('string_translation'));
    $class->setCurrencyFormatter($container->get('commerce_price.currency_formatter'));
    $class->setElementInfoManager($container->get('plugin.manager.element_info'));
    $class->setCurrentLocale($container->get('commerce.current_locale'));
    $class->setMessenger($container->get('messenger'));
    return $class;
  }

  /**
   * Constructor the manual refund payment receive form.
   *
   * @param \Drupal\commerce_partial_payments\OrderItemTrackingInterface $payment_tracking
   *   The payment tracking service.
   */
  public function __construct(OrderItemTrackingInterface $payment_tracking) {
    $this->paymentTracking = $payment_tracking;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $form['#success_message'] = t('Refund sent.');
    $form['amount'] = [
      '#type' => 'commerce_price',
      '#title' => t('Amount'),
      '#default_value' => $payment->getAmount()->multiply('-1')->toArray(),
      '#required' => TRUE,
    ];

    $this->alterPartialPayments($form, $form_state);

    return $form;
  }

  /**
   * Alter the receive payment form for partial payments.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see \Drupal\commerce_partial_payments\Plugin\Commerce\InlineForm\PartialPaymentsGatewayForm::alterReceiveForm
   */
  protected function alterPartialPayments(array &$inline_form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    // Don't do anything if the field doesn't exist.
    if (!$payment->hasField('order_item_tracking')) {
      return;
    }

    $payment_amount = $payment->getAmount();
    $order = $payment->getOrder();
    $paid = $order->getTotalPaid();

    $this->swapAmountForAllocationChoice($inline_form, $this->t('send'));

    if (!$payment->get('order_item_tracking')->isEmpty()) {
      $full_amount = $payment_amount->multiply('-1');
      $full_label = $this->t('Send the full @amount', [
        '@amount' => $this->currencyFormatter->format($full_amount->getNumber(), $full_amount->getCurrencyCode()),
      ]);
      TrackingElementTrait::addAllocationOption($inline_form['partial_payments_type'], 'full', $full_label, $full_amount, $payment->get('order_item_tracking')->getValue(), -50);
      $inline_form['partial_payments_type']['#default_value'] = 'full';
    }

    // Add the specific tracking.
    $inline_form['order_item_tracking'] = [
      '#type' => 'commerce_partial_payments_tracked_amount',
      '#tracking' => $this->paymentTracking->getTrackedAmountsForOrder($order),
      '#items' => [],
      '#empty' => $this->t('This order has no items.'),
      '#states' => [
        'visible' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
        'required' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
      ],
      '#weight' => 50,
    ];
    /** @var \Drupal\commerce_price\Price $amount */
    foreach ($payment->get('order_item_tracking')->getTrackedAmounts() as $target_id => $amount) {
      $amount = $amount->multiply($amount->isPositive() ? '0' : '-1');
      $inline_form['order_item_tracking']['#default_value'][] = [
        'target_id' => $target_id,
      ] + $amount->toArray();
    }
    foreach ($order->getItems() as $item) {
      $inline_form['order_item_tracking']['#items'][$item->id()] = [
        'label' => $item->label(),
        'price' => $item->getAdjustedTotalPrice(),
      ];
    }

    // Set the order balance so we can dynamically show a warning.
    TrackingElementTrait::addTrackingJs($inline_form, [
      'warning' => $paid ? $paid->getNumber() : 0,
    ]);

    // Add the warning element, hidden by default.
    $inline_form['amount_warning'] = [
      '#theme_wrappers' => ['container'],
      '#weight' => 90,
      '#theme' => 'status_messages',
      '#message_list' => [
        'warning' => [
          $this->t('You are about to refund more than has been paid on the order.'),
        ],
      ],
      '#attributes' => [
        'class' => [
          'cpp-tracking-warning',
          'hidden',
        ],
      ],
    ];

    $inline_form['#cpp_validate'][] = '::buildPaymentTracking';
  }

}
