<?php

namespace Drupal\contacts_events\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field Formatter to show date once if date range starts and ends on same day.
 *
 * @FieldFormatter(
 *   id = "contacts_events_date_range_compact",
 *   label = @Translation("Compact"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangeCompact extends DateRangeDefaultFormatter implements ContainerFactoryPluginInterface {

  /**
   * The date range formatter service.
   *
   * @var \Drupal\contacts_events\DateRangeFormatterInterface
   */
  protected $formatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $self = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $self->formatter = $container->get('contacts_events.date_range_compact.formatter');
    return $self;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->start_date) && !empty($item->end_date)) {
        $start_timestamp = $item->start_date->getTimestamp();
        $end_timestamp = $item->end_date->getTimestamp();
        $format = $this->getSetting('format_type');

        $timezone = date_default_timezone_get();
        $text = $this->formatter->formatDateTimeRange($start_timestamp, $end_timestamp, $format, $timezone, $langcode);

        $elements[$delta] = [
          '#plain_text' => $text,
          '#cache' => [
            'contexts' => [
              'timezone',
            ],
          ],
        ];
      }
    }

    return $elements;
  }

}
