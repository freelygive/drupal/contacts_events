<?php

namespace Drupal\contacts_events\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the text overrides field type.
 *
 * @FieldType(
 *   id = "contacts_events_text_overrides",
 *   label = @Translation("Text overrides"),
 *   category = @Translation("Events"),
 *   default_widget = "contacts_events_text_overrides",
 * )
 */
class TextOverridesItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = [
      'allowed_keys' => [
        'more_info' => new TranslatableMarkup('More info'),
        'book' => new TranslatableMarkup('Book now'),
      ],
    ];
    return $settings + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $settings = $this->getSettings();

    $default_values = [];
    foreach ($settings['allowed_keys'] as $key => $label) {
      $default_values[] = "{$key}|{$label}";
    }
    $element['foo'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed keys'),
      '#default_value' => implode("\n", $default_values),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->text === NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Name'));
    $properties['text'] = DataDefinition::create('string')
      ->setLabel(t('Text'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $options['name']['NotBlank'] = [];

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', $options);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'name' => [
        'type' => 'varchar',
        'length' => 255,
      ],
      'text' => [
        'type' => 'varchar',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['name'] = array_rand(['more_info', 'book']);
    $values['text'] = $random->word(mt_rand(1, 255));
    return $values;
  }

}
