<?php

namespace Drupal\contacts_events\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides Booking relation icons field handler.
 *
 * @ViewsField("contacts_events_ticketholder_ticket_status")
 */
class TicketStatus extends BookingInfoBase {

  /**
   * {@inheritdoc}
   */
  protected function renderBooking(OrderInterface $booking, $uid) {
    foreach ($booking->getItems() as $item) {
      if ($item->bundle() != 'contacts_ticket') {
        continue;
      }

      /** @var \Drupal\contacts_events\Entity\TicketInterface $ticket */
      $ticket = $item->getPurchasedEntity();

      if ($ticket->get('contact')->target_id == $uid) {
        return $item->get('state')->view(['label' => 'hidden']);
      }
    }

    return [];
  }

}
