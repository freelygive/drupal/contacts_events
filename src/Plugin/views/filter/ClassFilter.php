<?php

namespace Drupal\contacts_events\Plugin\views\filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter handler for class.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("contacts_events_class")
 */
class ClassFilter extends InOperator {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The argument used to build the value options.
   *
   * This is used to make sure we rebuild the options if the available argument
   * is changed.
   *
   * @var mixed
   */
  protected $eventId;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);
    $form['value']['#description'] = $this->t('If this view uses an argument for the event, this list will be filtered to the enabled set of classes for the event when an argument is present.');
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    // Reset the options if our event ID is different.
    $event_id = $this->getEventId();
    if ($event_id != $this->eventId) {
      $this->valueOptions = NULL;
    }

    if (isset($this->valueOptions)) {
      return $this->valueOptions;
    }

    // If we have an argument to pull the filtered list from, populate the
    // value options.
    if ($event_id) {
      /** @var \Drupal\contacts_events\Entity\Event $event */
      $event = $this->entityTypeManager->getStorage('contacts_event')->load($event_id);

      if ($event->hasField('ticket_classes')) {
        foreach ($event->get('ticket_classes') as $class_item) {
          /** @var \Drupal\contacts_events\Entity\EventClass $ticket_class */
          if ($event_class = $class_item->entity) {
            $this->valueOptions[$event_class->id()] = $event_class->label();
          }
          else {
            $this->valueOptions[$class_item->target_id] = new TranslatableMarkup(
              'Missing class %id',
              ['%id' => $class_item->target_id],
            );
          }
        }
      }
    }
    // Otherwise just return the global list without storing it.
    else {
      foreach ($this->entityTypeManager->getStorage('contacts_events_class')->loadMultiple() as $id => $class) {
        if (in_array($class->get('type'), ['global', 'contacts_ticket'])) {
          $this->valueOptions[$id] = $class->label();
        }
      }
    }

    return $this->valueOptions;
  }

  /**
   * Get the event ID we are filtering on.
   *
   * @return int|null
   *   The event ID or NULL if we can't find the argument.
   */
  protected function getEventId() {
    if (isset($this->view->argument)) {
      foreach ($this->view->argument as $argument) {
        if ($argument->table == 'contacts_ticket' && $argument->field == 'event') {
          $arg = $argument->getValue();
          if ($arg && $argument->validateArgument($arg)) {
            return $arg;
          }
          return NULL;
        }
      }
    }
    return NULL;
  }

}
