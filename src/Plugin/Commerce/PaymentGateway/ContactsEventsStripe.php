<?php

namespace Drupal\contacts_events\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe\ErrorHelper;
use Drupal\commerce_stripe\Event\PaymentIntentEvent;
use Drupal\commerce_stripe\Event\StripeEvents;
use Drupal\commerce_stripe\Plugin\Commerce\PaymentGateway\Stripe;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\TempStore\PrivateTempStore;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override for the Stripe gateway to handle deposits/other partial payments.
 */
class ContactsEventsStripe extends Stripe {

  /**
   * The booking payment private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $tempstore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->tempstore = $container->get('tempstore.private')->get('contacts_events.booking_flow');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentIntent(OrderInterface $order, $intent_attributes = [], PaymentInterface $payment = NULL) {
    if (is_bool($intent_attributes)) {
      $intent_attributes = [
        'capture_method' => $intent_attributes ? 'automatic' : 'manual',
      ];
      @trigger_error('Passing a boolean representing capture method as the second parameter to StripeInterface::createPaymentIntent() is deprecated in commerce_stripe:8.x-1.0 and this parameter must be an array of payment intent attributes in commerce_stripe:9.x-2.0. See https://www.drupal.org/project/commerce_stripe/issues/3259211', E_USER_DEPRECATED);
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $payment ? $payment->getPaymentMethod() : $order->get('payment_method')->entity;
    /** @var \Drupal\commerce_price\Price */
    $amount = $this->getPaymentAmount($order);

    $default_intent_attributes = [
      'amount' => $this->minorUnitsConverter->toMinorUnits($amount),
      'currency' => strtolower($amount->getCurrencyCode()),
      'payment_method_types' => ['card'],
      'metadata' => [
        'order_id' => $order->id(),
        'store_id' => $order->getStoreId(),
      ],
      'payment_method' => $payment_method->getRemoteId(),
      'capture_method' => 'automatic',
    ];

    $customer_remote_id = $this->getRemoteCustomerId($order->getCustomer());
    if (!empty($customer_remote_id)) {
      $default_intent_attributes['customer'] = $customer_remote_id;
    }

    $intent_array = NestedArray::mergeDeep($default_intent_attributes, $intent_attributes);

    // Add metadata and extra transaction data where required.
    $event = new PaymentIntentEvent($order, $intent_array);
    $this->eventDispatcher->dispatch($event, StripeEvents::PAYMENT_INTENT_CREATE);

    // Alter or extend the intent array from additional information added
    // through the event.
    $intent_array = $event->getIntentAttributes();

    try {
      $intent = PaymentIntent::create($intent_array);
      $order->setData('stripe_intent', $intent->id)->save();
    }
    catch (ApiErrorException $e) {
      ErrorHelper::handleException($e);
    }
    return $intent;
  }

  /**
   * Get the payment amount from the private tempstore.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment amount, or NULL if there is none specified.
   */
  protected function getPaymentAmount(OrderInterface $order): ?Price {
    $amount = $this->tempstore->get("{$order->id()}:payment_amount");
    return $amount ? Price::fromArray($amount) : $order->getBalance();
  }

}
