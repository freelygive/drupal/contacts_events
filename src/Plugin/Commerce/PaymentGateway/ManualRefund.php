<?php

namespace Drupal\contacts_events\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_price\Price;

/**
 * Provides the Manual refund payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "manual_refund",
 *   label = "Manual refund",
 *   display_label = "Manual refund",
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   },
 *   forms = {
 *     "add-payment" =
 *   "Drupal\contacts_events\PluginForm\ManualRefundPaymentAddForm",
 *     "receive-payment" =
 *   "Drupal\contacts_events\PluginForm\ManualRefundPaymentReceiveForm",
 *   },
 *   payment_type = "payment_manual",
 * )
 */
class ManualRefund extends PaymentGatewayBase implements ManualRefundPaymentGatewayInterface {

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    $operations = [];
    $operations['receive'] = [
      'title' => $this->t('Send refund'),
      'page_title' => $this->t('Mark as sent'),
      'plugin_form' => 'receive-payment',
      'access' => $payment_state == 'pending',
    ];
    $operations['void'] = [
      'title' => $this->t('Void'),
      'page_title' => $this->t('Void payment'),
      'plugin_form' => 'void-payment',
      'access' => $payment_state == 'pending',
    ];

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $received = FALSE) {
    $this->assertPaymentState($payment, ['new']);

    // Negate the payment amount.
    $payment->setAmount($payment->getAmount()->multiply('-1'));

    if ($payment->hasField('order_item_tracking')) {
      /** @var \Drupal\commerce_partial_payments\Plugin\Field\FieldType\CommerceTrackedAmountItem $item */
      foreach ($payment->get('order_item_tracking') as $item) {
        $item->number = $item->toPrice()->multiply('-1')->getNumber();
      }
    }

    $payment->state = $received ? 'completed' : 'pending';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['pending']);

    // Negate the specified amount, otherwise use the entire amount.
    $amount = $amount ? $amount->multiply('-1') : $payment->getAmount();

    // See if we need to negate the tracked amounts.
    if ($payment->hasField('order_item_tracking')) {
      /** @var \Drupal\commerce_price\Price|null $total */
      $total = NULL;
      /** @var \Drupal\commerce_price\Price $item_amount */
      foreach ($payment->get('order_item_tracking')->getTrackedAmounts() as $item_amount) {
        $total = $total ? $total->add($item_amount) : $item_amount;
      }

      // If the tracking is inverted, we can fix it.
      if (!$total->equals($amount) && $total->equals($amount->multiply('-1'))) {
        /** @var \Drupal\commerce_partial_payments\Plugin\Field\FieldType\CommerceTrackedAmountItem $item */
        foreach ($payment->get('order_item_tracking') as $item) {
          $item->number = $item->toPrice()->multiply('-1')->getNumber();
        }
      }
    }

    $payment->state = 'completed';
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['pending']);

    $payment->state = 'voided';
    $payment->save();
  }

}
