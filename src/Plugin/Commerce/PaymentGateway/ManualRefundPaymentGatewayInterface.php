<?php

namespace Drupal\contacts_events\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;

/**
 * Provides the base interface for manual refund payment gateways.
 *
 * Manual refund payments are admin only and post a negative payment.
 *
 * Examples: cash on delivery, pay in person, cheque, bank transfer, etc.
 */
interface ManualRefundPaymentGatewayInterface extends PaymentGatewayInterface, SupportsVoidsInterface {

  /**
   * Creates a payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param bool $received
   *   Whether the payment was already received.
   */
  public function createPayment(PaymentInterface $payment, $received = FALSE);

  /**
   * Receives the given payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param \Drupal\commerce_price\Price $amount
   *   The received amount. If NULL, defaults to the entire payment amount.
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL);

}
