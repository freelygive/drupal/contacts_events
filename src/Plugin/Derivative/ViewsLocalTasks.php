<?php

namespace Drupal\contacts_events\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver to validate that a view exists for a local task.
 */
class ViewsLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The view entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Creates an ViewsLocalTasks object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('view');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Only add the derivative if the view and display exist and are enabled.
    // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
    [, $view_id, $display_id] = explode('.', $base_plugin_definition['route_name']);
    /** @var \Drupal\views\ViewEntityInterface $view */
    if ($view = $this->storage->load($view_id)) {
      if ($view->status() && $display = $view->getDisplay($display_id)) {
        if ($display['display_options']['enabled'] ?? TRUE) {
          $this->derivatives['view'] = $base_plugin_definition;
        }
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
