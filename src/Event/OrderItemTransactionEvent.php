<?php

namespace Drupal\contacts_events\Event;

use Drupal\bookkeeping\Event\OrderTransactionEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;

/**
 * Event raised when preparing a transaction posting for a payment.
 */
class OrderItemTransactionEvent extends OrderTransactionEvent {

  /**
   * The event name for the commerce payment transaction event.
   */
  const EVENT = 'contacts_events_commerce_order_item_transaction';

  /**
   * The payment this relates to.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface
   */
  protected $orderItem;

  /**
   * The original payment this relates to, if any.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface|null
   */
  protected $originalOrderItem;

  /**
   * Construct the Payment Transaction event.
   *
   * @param string $generator
   *   The generator.
   * @param \Drupal\commerce_price\Price $value
   *   The value we are posting.
   * @param string|null $from
   *   The account to post from (credit).
   * @param string|null $to
   *   The account to post to (debit).
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order this transaction relates to.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item this transaction relates to.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface|null $original_order_item
   *   The original order item this transaction relates to, if any.
   */
  public function __construct(string $generator, Price $value, ?string $from, ?string $to, OrderInterface $order, OrderItemInterface $order_item, ?OrderItemInterface $original_order_item) {
    parent::__construct($generator, $value, $from, $to, $order, NULL);
    $this->orderItem = $order_item;
    $this->originalOrderItem = $original_order_item;
    $this->related[] = $order_item;
  }

  /**
   * Get the order item.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface
   *   The order item.
   */
  public function getOrderItem(): OrderItemInterface {
    return $this->orderItem;
  }

  /**
   * Get the order item.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface|null
   *   The order item, if any.
   */
  public function getOriginalOrderItem(): ?OrderItemInterface {
    return $this->originalOrderItem;
  }

}
