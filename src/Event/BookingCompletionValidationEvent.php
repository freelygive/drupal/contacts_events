<?php

namespace Drupal\contacts_events\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event raised prior to attempting booking completion (payment/confirmation).
 */
class BookingCompletionValidationEvent extends Event {

  /**
   * The event name.
   */
  const NAME = 'contacts_events.booking_completion_validation';

  /**
   * The booking.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Whether soft errors should be added as errors.
   *
   * @var bool
   */
  protected $soft;

  /**
   * Warning messages.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  protected $warnings = [];

  /**
   * Error messages.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  protected $errors = [];

  /**
   * Construct the booking completion validation event.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The booking.
   * @param bool $soft
   *   Whether soft errors should be added as warnings.
   */
  public function __construct(OrderInterface $order, bool $soft) {
    $this->order = $order;
    $this->soft = $soft;
  }

  /**
   * Get the booking.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The booking.
   */
  public function getOrder() : OrderInterface {
    return $this->order;
  }

  /**
   * Add a warning to the payment step.
   *
   * @param \Drupal\Component\Render\MarkupInterface|string $message
   *   The message to add.
   */
  public function addWarning($message) {
    $this->warnings[] = $message;
  }

  /**
   * Whether there are warnings for this booking.
   *
   * @return bool
   *   Whether there are warnings.
   */
  public function hasWarnings() : bool {
    return count($this->warnings) > 0;
  }

  /**
   * Get any warnings.
   *
   * @return array
   *   The warnings.
   */
  public function getWarnings() : array {
    return $this->warnings;
  }

  /**
   * Add an error to the payment step.
   *
   * @param \Drupal\Component\Render\MarkupInterface|string $message
   *   The message to add.
   */
  public function addError($message) {
    $this->errors[] = $message;
  }

  /**
   * Whether there are errors for this booking.
   *
   * @return bool
   *   Whether there are errors.
   */
  public function hasErrors() : bool {
    return count($this->errors) > 0;
  }

  /**
   * Get any errors.
   *
   * @return array
   *   The errors.
   */
  public function getErrors() : array {
    return $this->errors;
  }

  /**
   * Add a soft error to the payment step.
   *
   * A soft error will show a warning for users with the manage permission.
   *
   * @param \Drupal\Component\Render\MarkupInterface|string $message
   *   The message to add.
   */
  public function addSoftError($message) {
    if ($this->soft) {
      $this->warnings[] = $message;
    }
    else {
      $this->errors[] = $message;
    }
  }

}
