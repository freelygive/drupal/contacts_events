<?php

namespace Drupal\contacts_events\Event;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Event raised when a ticket price is calculated.
 *
 * @package Drupal\contacts_events\Event
 */
class PriceCalculationEvent extends Event {

  /**
   * The event name.
   */
  const NAME = 'contacts_events.price_calculation';

  /**
   * The order item.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface
   */
  protected $orderItem;

  /**
   * Price map.
   *
   * @var \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItem[][]|null
   */
  protected $priceMap;

  /**
   * The Mapped price.
   *
   * @var array|null
   *   An associative array containing:
   *   - booking_window: The booking window, NULL if none found.
   *   - booking_window_overridden: Boolean, whether the window is overridden.
   *   - class: The event class, NULL if none found.
   *   - class_overridden: Boolean, whether the class is overridden.
   */
  protected $mapping;

  /**
   * The purchased entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $purchasedEntity;

  /**
   * The originally calculated price.
   *
   * @var \Drupal\commerce_price\Price|null
   */
  protected $calculatedPrice;

  /**
   * The new price.
   *
   * @var \Drupal\commerce_price\Price|null
   */
  protected $currentPrice;

  /**
   * PriceCalculationEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   * @param \Drupal\Core\Entity\EntityInterface $purchased_entity
   *   Purchased entity (ticket)
   * @param \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItem[][]|null $price_map
   *   Price map.
   * @param array|null $mapping
   *   Mapped price.
   * @param \Drupal\commerce_price\Price|null $calculated_price
   *   Originally calculated price.
   */
  public function __construct(OrderItemInterface $order_item, EntityInterface $purchased_entity, array $price_map = NULL, array $mapping = NULL, Price $calculated_price = NULL) {
    $this->orderItem = $order_item;
    $this->priceMap = $price_map;
    $this->mapping = $mapping;
    $this->purchasedEntity = $purchased_entity;
    $this->calculatedPrice = $calculated_price;
    $this->currentPrice = $calculated_price;
  }

  /**
   * Gets the order item.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface
   *   The order item.
   */
  public function getOrderItem(): OrderItemInterface {
    return $this->orderItem;
  }

  /**
   * Gets the price map.
   *
   * @return \Drupal\contacts_events\Plugin\Field\FieldType\PriceMapItem[][]|null
   *   The price map.
   */
  public function getPriceMap(): ?array {
    return $this->priceMap;
  }

  /**
   * Gets the mapping.
   *
   * @return array|null
   *   An associative array containing:
   *   - booking_window: The booking window, NULL if none found.
   *   - booking_window_overridden: Boolean, whether the window is overridden.
   *   - class: The event class, NULL if none found.
   *   - class_overridden: Boolean, whether the class is overridden.
   */
  public function getMapping(): ?array {
    return $this->mapping;
  }

  /**
   * Gets the purchased entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The purchased entity.
   */
  public function getPurchasedEntity(): EntityInterface {
    return $this->purchasedEntity;
  }

  /**
   * Gets the calculated price.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The calculated price.
   */
  public function getCalculatedPrice(): ?Price {
    return $this->calculatedPrice;
  }

  /**
   * Gets the current price.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The current price.
   */
  public function getPrice(): ?Price {
    return $this->currentPrice;
  }

  /**
   * Sets the new price.
   *
   * @param \Drupal\commerce_price\Price $new_price
   *   The new price.
   */
  public function setPrice(Price $new_price) {
    $this->currentPrice = $new_price;
  }

}
