<?php

namespace Drupal\contacts_events\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\user\UserInterface;

/**
 * Event raised when a ticket is pre-populated.
 *
 * @package Drupal\contacts_events\Event
 */
class TicketPrepopulateEvent extends Event {
  /**
   * The event name.
   */
  const NAME = 'contacts_events.ticket.pre_populate';

  /**
   * The ticket.
   *
   * @var \Drupal\contacts_events\Entity\TicketInterface
   */
  public TicketInterface $ticket;

  /**
   * The user's individual profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  public $user;

  /**
   * TicketPrepopulateEvent constructor.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket.
   * @param \Drupal\user\UserInterface $user
   *   The individual profile.
   */
  public function __construct(TicketInterface $ticket, UserInterface $user) {
    $this->user = $user;
    $this->ticket = $ticket;
  }

}
