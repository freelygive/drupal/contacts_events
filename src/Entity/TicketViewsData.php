<?php

namespace Drupal\contacts_events\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Ticket entities.
 */
class TicketViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['contacts_ticket']['mapped_price__class']['filter']['id'] = 'contacts_events_class';

    // Set up the full name field and filter.
    $keys = ['title', 'help', 'field', 'entity field'];
    $data['contacts_ticket']['name'] = array_intersect_key($data['contacts_ticket']['name__title'], array_fill_keys($keys, TRUE));
    $data['contacts_ticket']['name__title']['title'] = $data['contacts_ticket']['name__title']['title'] . ' (Title)';
    $data['contacts_ticket']['name']['filter'] = [
      'table' => 'contacts_ticket',
      'field_name' => 'name',
      'id' => 'name_fulltext',
      'allow_empty' => TRUE,
      // Work around \Drupal\name\Plugin\views\filter\Fulltext::query not being
      // compatible with base fields that use double underscores.
      'real field' => 'name_',
    ];
    // Remove the field from all the other columns.
    unset($data['contacts_ticket']['name__title']['field']);
    unset($data['contacts_ticket']['name__given']['field']);
    unset($data['contacts_ticket']['name__middle']['field']);
    unset($data['contacts_ticket']['name__family']['field']);
    unset($data['contacts_ticket']['name__generational']['field']);
    unset($data['contacts_ticket']['name__credentials']['field']);

    return $data;
  }

}
