<?php

namespace Drupal\contacts_events\Entity;

use Drupal\contacts_events\Event\CloneEvent;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Defines the Event entity.
 *
 * @ingroup contacts_events
 *
 * @ContentEntityType(
 *   id = "contacts_event",
 *   label = @Translation("Event"),
 *   bundle_label = @Translation("Event type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contacts_events\EventListBuilder",
 *     "views_data" = "Drupal\contacts_events\Entity\EventViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\contacts_events\Form\EventForm",
 *       "add" = "Drupal\contacts_events\Form\EventForm",
 *       "edit" = "Drupal\contacts_events\Form\EventForm",
 *       "clone" = "Drupal\contacts_events\Form\EventCloneForm",
 *       "delete" = "Drupal\contacts_events\Form\EventDeleteForm",
 *     },
 *     "access" = "Drupal\contacts_events\EventAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events\EventHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "contacts_event",
 *   admin_permission = "administer contacts_event entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/event/{contacts_event}",
 *     "add-page" = "/event/add",
 *     "add-form" = "/event/add/{event_type}",
 *     "edit-form" = "/event/{contacts_event}/edit",
 *     "clone-form" = "/event/{contacts_event}/clone",
 *     "delete-form" = "/event/{contacts_event}/delete",
 *     "collection" = "/admin/content/event",
 *     "book" = "/event/{contacts_event}/book",
 *   },
 *   bundle_entity_type = "event_type",
 *   field_ui_base_route = "entity.event_type.edit_form"
 * )
 */
class Event extends ContentEntityBase implements EventInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isBookingEnabled() {
    return $this->get('booking_status')->value != self::STATUS_DISABLED;
  }

  /**
   * {@inheritdoc}
   */
  public function isBookingOpen() {
    // Always FALSE if the booking status is not open.
    if ($this->get('booking_status')->value !== self::STATUS_OPEN) {
      return FALSE;
    }

    // Otherwise bookings are open for events that end in the future.
    /** @var \Drupal\Core\Datetime\DrupalDateTime|null $end */
    $end = $this->get('date')->end_date;
    return !$end || $end->getTimestamp() > \Drupal::service('datetime.time')->getRequestTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key, $default = NULL) {
    return $this->get('settings')->getSettingValue($key, $default);
  }

  /**
   * {@inheritdoc}
   */
  public function setSetting($key, $value) {
    $this->get('settings')->setSettingValue($key, $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetSetting($key) {
    $this->get('settings')->unsetSettingValue($key);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Code'))
      ->setDescription(new TranslatableMarkup('Short code for the event, also used to prefix booking numbers.'))
      ->setSettings([
        'max_length' => 31,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tagline'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Tagline'))
      ->setDescription(new TranslatableMarkup('Tagline for the event.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date'] = BaseFieldDefinition::create('daterange')
      ->setLabel(new TranslatableMarkup('Date'))
      ->setDescription(new TranslatableMarkup('Date and time of the event.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['venue'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Venue'))
      ->setDescription(new TranslatableMarkup('Location of the event.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['price'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Starting Price'))
      ->setDescription(new TranslatableMarkup('The cheapest price for this event.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['listing_image'] = BaseFieldDefinition::create('image')
      ->setLabel(new TranslatableMarkup('Listing image'))
      ->setDescription(new TranslatableMarkup('Smaller image used in the event listings.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['banner_image'] = BaseFieldDefinition::create('image')
      ->setLabel(new TranslatableMarkup('Full Banner'))
      ->setDescription(new TranslatableMarkup('Full width banner image for the event.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Published'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'));

    $fields['booking_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Is this event open for bookings?'))
      ->setRequired(TRUE)
      ->setDefaultValue(self::STATUS_DISABLED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('allowed_values', [
        self::STATUS_OPEN => new TranslatableMarkup('Open for bookings'),
        self::STATUS_CLOSED => new TranslatableMarkup('Closed for new bookings'),
        self::STATUS_DISABLED => new TranslatableMarkup('Bookings disabled'),
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['link_overrides'] = BaseFieldDefinition::create('contacts_events_text_overrides')
      ->setLabel('Link text overrides')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    // Put this as a bundle field for all bundles to force a separate storage
    // table.
    $fields['settings'] = BundleFieldDefinition::create('contacts_events_settings')
      ->setLabel('Settings')
      ->setSetting('properties', [])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayOptions('view', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', ['region' => 'hidden']);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    /** @var static $duplicate */
    $duplicate = parent::createDuplicate();

    $duplicate->title = new TranslatableMarkup('Clone of @title', [
      '@title' => $duplicate->title->value,
    ]);
    $duplicate->code = NULL;
    $duplicate->date = NULL;
    $duplicate->status = FALSE;
    $duplicate->booking_status = self::STATUS_DISABLED;
    $duplicate->setSetting('clone', [
      'status' => CloneEvent::STATUS_PENDING,
      'progress' => 0,
      'source' => $this->id(),
    ]);

    return $duplicate;
  }

}
