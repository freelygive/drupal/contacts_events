<?php

namespace Drupal\contacts_events\Form;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Event\CloneEvent;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Contacts Events form.
 */
class EventCloneForm extends EventForm {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The clone queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->eventDispatcher = $container->get('event_dispatcher');
    $form->queue = $container->get('queue')->get('contacts_events_clone');
    return $form;
  }

  /**
   * Get the title for the form page.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event being cloned.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function title(EventInterface $contacts_event) {
    return $this->t('Clone %event', [
      '%event' => $contacts_event->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    // We want to set a duplicate rather than the actual entity.
    return parent::setEntity($entity->createDuplicate());
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['_clone_info'] = [
      '#type' => 'container',
      'message' => [
        '#markup' => $this->t('The new event will be created immediately, but additional configuration will be replicated on cron. It will not be possible to edit or open for bookings until that is completed.'),
      ],
      '#attributes' => ['class' => ['messages', 'messages--warning']],
      '#weight' => 99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Clone event');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    $source = $this->entityTypeManager
      ->getStorage('contacts_event')
      ->load($this->entity->getSetting('clone.source'));

    // Trigger the clone event for other parts of the system to hook into.
    $clone_event = new CloneEvent($this->entity, $source->id());
    $this->eventDispatcher->dispatch($clone_event, CloneEvent::class);

    // If there are operations left to perform, queue the event set a message.
    if ($clone_event->getProgress() < 100) {
      $this->queue->createItem($clone_event);

      $this->messenger()->addWarning($this->t('%label is cloning from %source, please allow it to finish before editing or opening for bookings.', [
        '%label' => $this->entity->label(),
        '%source' => $source->label(),
      ]));
    }
    // Otherwise update the clone progress on the event.
    else {
      $this->entity
        ->setSetting('clone.status', CloneEvent::STATUS_COMPLETED)
        ->save();

      $this->messenger()->addStatus($this->t('%label has finished cloning from %source and may be opened for bookings.', [
        '%label' => $this->entity->label(),
        '%source' => $source->label(),
      ]));
    }
  }

}
