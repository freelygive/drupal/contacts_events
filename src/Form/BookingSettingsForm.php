<?php

namespace Drupal\contacts_events\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The booking settings form.
 */
class BookingSettingsForm extends ConfigFormBase {

  /**
   * The store storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $storeStorage;

  /**
   * Constructs a new BookingSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->storeStorage = $entity_type_manager->getStorage('commerce_store');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'contacts_events.booking_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_events_booking_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contacts_events.booking_settings');

    $form['store_id'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Booking Store'),
      '#description' => $this->t('Which store to associate with bookings.'),
      '#target_type' => 'commerce_store',
      '#default_value' => $config->get('store_id'),
      '#required' => TRUE,
      '#hide_single_entity' => FALSE,
    ];

    $form['autofill_first_ticket'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto-fill first ticket'),
      '#description' => $this->t("Auto-fill first ticket with booking manager's details"),
      '#default_value' => $config->get('autofill_first_ticket'),
    ];

    $form['capacity_hard_limit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enforce Event capacity'),
      '#description' => $this->t("Prevent users buying tickets for events that have reached capacity"),
      '#default_value' => $config->get('capacity_hard_limit'),
    ];

    $form['admin_payment_confirmation'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Admin payment confirmation'),
      '#tree' => TRUE,
      'subject' => [
        '#type' => 'textfield',
        '#title' => $this->t('Subject'),
        '#description' => $this->t('The subject of the confirmation email'),
        '#default_value' => $config->get('admin_payment_confirmation.subject'),
      ],
      'email' => [
        '#type' => 'text_format',
        '#title' => $this->t('Admin payment confirmation'),
        '#description' => $this->t('Email contents when an admin user creates a payment for another user.'),
        '#default_value' => $config->get('admin_payment_confirmation.email.value'),
        '#format' => $config->get('admin_payment_confirmation.email.format') ?? filter_default_format(),
      ],
      'tokens' => [
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'site',
          'contacts_event',
          'commerce_order',
          'commerce_payment',
        ],
        '#global_types' => FALSE,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('contacts_events.booking_settings')
      ->set('store_id', $form_state->getValue('store_id'))
      ->set('autofill_first_ticket', $form_state->getValue('autofill_first_ticket'))
      ->set('capacity_hard_limit', $form_state->getValue('capacity_hard_limit'))
      ->set('admin_payment_confirmation', $form_state->getValue('admin_payment_confirmation'))
      ->save();
  }

}
