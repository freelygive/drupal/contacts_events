<?php

namespace Drupal\contacts_events\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Form alter handler for the booking admin additional charges form.
 */
class BookingAdminAdditionalChargesFormAlter {

  /**
   * Form alter for the booking admin additional charges form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $form_id
   *   The form ID.
   */
  public function alter(array &$form, FormStateInterface $form_state, string $form_id) : void {
    // Disable the outer form actions - the tickets are auto saved and there
    // should be nothing else on this page, otherwise it gets confusing.
    $form['actions']['#access'] = FALSE;

    // Grab the widget form so we don't have to keep using the full path.
    $widget = &$form['order_items']['widget'];

    // Remove the type to drop the fieldset.
    unset($widget['#type']);

    // Move the actions to the top of the page.
    if (isset($widget['actions'])) {
      $widget['actions']['#weight'] = -99;

      // Convert into an actions, removing the inline container class.
      $widget['actions']['#type'] = 'actions';
      unset($widget['actions']['#attributes']);

      // Set the primary class on the add button.
      if (isset($widget['actions']['ief_add'])) {
        $widget['actions']['ief_add']['#attributes']['class'][] = 'button';
        $widget['actions']['ief_add']['#attributes']['class'][] = 'button--primary';
      }
    }

    // Add the process callback to the general IEF.
    if (isset($widget['form']['inline_entity_form'])) {
      // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
      $widget['form']['inline_entity_form']['#process'][] = [$this, 'processIefForm'];
    }

    // Modify the entity rows.
    if (isset($widget['entities'])) {
      foreach (Element::children($widget['entities']) as $key) {
        $row = &$widget['entities'][$key];

        // Demote the cancel button to a 'danger' button.
        if (isset($row['actions']['cancel'])) {
          $row['actions']['cancel']['#attributes']['class'][] = 'button';
          $row['actions']['cancel']['#attributes']['class'][] = 'button--danger';
        }

        // Add the process callback to the entity IEF.
        if (isset($row['form']['inline_entity_form'])) {
          // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
          $row['form']['inline_entity_form']['#process'][] = [$this, 'processIefForm'];
        }
      }
    }
  }

  /**
   * Process callback for the IEF ticket form.
   *
   * @param array $element
   *   The EIF ticket form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element array.
   */
  public function processIefForm(array $element, FormStateInterface $form_state) : array {
    // Make the 'save' action for the operation the primary.
    if (isset($element['actions']["ief_{$element['#op']}_save"])) {
      $element['actions']["ief_{$element['#op']}_save"]['#attributes']['class'][] = 'button';
      $element['actions']["ief_{$element['#op']}_save"]['#attributes']['class'][] = 'button--primary';
    }

    // Remove the inner container and fieldset.
    unset($element['purchased_entity']['#type']);
    unset($element['purchased_entity']['widget'][0]['#type']);

    return $element;
  }

}
