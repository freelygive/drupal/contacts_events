<?php

namespace Drupal\contacts_events;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Trait for common processes.
 */
trait OrderEventTransitionTrait {

  /**
   * Dispatch a workflow event.
   *
   * @param string $transition_id
   *   The name of the transition.
   * @param string $phase
   *   The phase, either pre_transition or post_transition.
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $original_event
   *   The original transition event.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being transitioned.
   */
  protected function dispatchTransitionEvent($transition_id, $phase, WorkflowTransitionEvent $original_event, ContentEntityInterface $entity) {
    $workflow = $original_event->getWorkflow();
    $transition = $workflow->getTransition($transition_id);

    if ($transition) {
      $group_id = $workflow->getGroup();
      $event = new WorkflowTransitionEvent($transition, $workflow, $entity, $original_event->getFieldName());
      $events = [
        // For example: 'commerce_order.place.pre_transition'.
        $group_id . '.' . $transition_id . '.' . $phase,
        // For example: 'commerce_order.pre_transition'.
        $group_id . '.' . $phase,
        // For example: 'state_machine.pre_transition'.
        'state_machine.' . $phase,
      ];
      foreach ($events as $event_id) {
        $this->eventDispatcher->dispatch($event, $event_id);
      }
    }
  }

}
