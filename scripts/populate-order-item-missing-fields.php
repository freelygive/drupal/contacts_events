<?php

/**
 * @file
 * Populate state and confirmed fields for various order item types.
 */

$to_update = \Drupal::state()->get('contacts_events_install_order_item_fields');

$io = $this->io();

if (empty($to_update)) {
  $io->warning('Nothing to update');
  return;
}

$last_id = 0;
$storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');
$query = $storage->getQuery();
$query->accessCheck();
$query->condition('type', array_keys($to_update), 'IN');
$or = $query->orConditionGroup();
$or->notExists('state');
$or->notExists('confirmed');
$query->condition($or);
$query->sort('order_item_id');

$total = (clone $query)->count()->execute();
$io->text("$total order items to update.");

$progress = $io->createProgressBar($total);
$progress->display();

do {
  $batch_query = clone $query;
  $batch_query->condition('order_item_id', $last_id, '>');
  $batch_query->range(0, 50);
  $ids = $batch_query->execute();

  foreach ($storage->loadMultiple($ids) as $order_item) {
    $order = $order_item->getOrder();

    if (empty($order_item->get('state')->value)) {
      if ($order->get('state')->value && $order->get('state')->value != 'draft') {
        $order_item->set('state', 'confirmed');
      }
      elseif ($order->get('state')->value) {
        $order_item->set('state', 'pending');
      }
    }

    if (empty($order_item->get('confirmed')->value)) {
      $time = $order->getPlacedTime() > $order_item->getCreatedTime()
        ? $order->getPlacedTime()
        : $order_item->getCreatedTime();

      $order_item->set('confirmed', $time);
    }

    $order_item->save();

    $progress->advance();
    $last_id = $order_item->id();
  }

  $storage->resetCache();
  sleep(2);
} while (!empty($ids));

$progress->clear();
$io->text('Done');
