/**
 * @file
 * Attaches behaviors for the Event forms.
 */
(function ($) {

  "use strict";

  /**
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.contactsEventFormSummaries = {
    attach: function () {
      $('[data-drupal-selector="edit-group-booking-settings"]').drupalSetSummary(function (context) {
        var $label = $(':input[name="booking_status"]:checked', context).next('label');
        if ($label.length) {
          return Drupal.checkPlain($label.text());
        }
        return Drupal.t('Status unknown');
      });
    }
  };

})(jQuery);
