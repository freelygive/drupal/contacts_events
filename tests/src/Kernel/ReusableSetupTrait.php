<?php

namespace Drupal\Tests\contacts_events\Kernel;

use Drupal\Core\Database\Database;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\TestRequirementsTrait;

/**
 * Trait for storing and reusing database fixtures for all tests within a class.
 */
trait ReusableSetupTrait {

  use TestRequirementsTrait;

  /**
   * The filename used for the database dump.
   *
   * @var string
   */
  protected $dumpFile;

  /**
   * Perform the setup tasks, using a cached fixture if possible.
   *
   * @see \Drupal\KernelTests\KernelTestBase::setUp()
   */
  protected function setUp(): void {
    $this->initDumpFile();
    if (file_exists($this->dumpFile)) {
      KernelTestBase::setUp();
      $this->restoreDatabase();
      drupal_flush_all_caches();
      $this->uncachedSetup();
    }
    else {
      parent::setUp();
      $this->cachedSetup();
      $this->dumpDatabase();
    }
  }

  /**
   * Perform set up actions that will be part of the cached database fixture.
   */
  protected function cachedSetup() {
    // For sub-classes to implement.
  }

  /**
   * Perform set up actions after the a cached database fixture is installed.
   */
  protected function uncachedSetup() {
    // For sub-classes to implement.
  }

  /**
   * Abstract for \Drupal\KernelTests\KernelTestBase::initFileCache().
   */
  abstract protected function initFileCache();

  /**
   * Abstract for \Drupal\KernelTests\KernelTestBase::bootEnvironment().
   */
  abstract protected function bootEnvironment();

  /**
   * Abstract for \Drupal\KernelTests\KernelTestBase::bootKernel().
   */
  abstract protected function bootKernel();

  /**
   * Abstract for \Drupal\KernelTests\KernelTestBase::getDatabasePrefix().
   */
  abstract protected function getDatabasePrefix();

  /**
   * Determines a proper file name for SQL dump.
   */
  protected function initDumpFile() {
    $class = get_class($this);
    $cache_dir = getenv('BROWSERTEST_CACHE_DIR') ?: sys_get_temp_dir() . '/test_dumps/' . \Drupal::VERSION;
    is_dir($cache_dir) || mkdir($cache_dir, 0777, TRUE);
    $this->dumpFile = $cache_dir . '/_' . preg_replace('/[^A-Za-z0-9\._]/', '_', $class) . '.sql';
  }

  /**
   * Dumps database structure and contents of test site.
   */
  protected function dumpDatabase() {
    $connection_info = Database::getConnectionInfo('default');

    $host = $connection_info['default']['host'];
    $port = $connection_info['default']['port'] ?? 3306;
    $user = $connection_info['default']['username'];
    $pass = $connection_info['default']['password'];
    $db = $connection_info['default']['database'];

    switch ($connection_info['default']['driver']) {
      case 'mysql':
        $tables = \Drupal::database()
          ->query("SHOW TABLES LIKE '{$this->getDatabasePrefix()}%'")
          ->fetchCol();
        $tables_param = implode(' ', $tables);
        exec("mysqldump -h $host -P $port -u$user -p$pass $db $tables_param | sed 's/{$this->getDatabasePrefix()}/default_db_prefix_/g' > {$this->dumpFile}");
        break;

      default:
        throw new \LogicException('This database driver is not supported yet.');
    }
  }

  /**
   * Restores database structure and contents of test site.
   */
  protected function restoreDatabase() {
    $connection_info = Database::getConnectionInfo('default');

    $host = $connection_info['default']['host'];
    $port = $connection_info['default']['port'] ?? 3306;
    $user = $connection_info['default']['username'];
    $pass = $connection_info['default']['password'];
    $db = $connection_info['default']['database'];

    switch ($connection_info['default']['driver']) {
      case 'mysql':
        exec("sed 's/default_db_prefix_/{$this->getDatabasePrefix()}/g' $this->dumpFile | mysql -h $host -P $port -u$user -p$pass $db", $output, $return_var);
        break;

      default:
        throw new \LogicException('This database driver is not supported yet.');
    }
  }

}
